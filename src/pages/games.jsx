import React, { useState, useEffect } from "react";
import Navbar from "../components/shared/Navbar";
import Footer from "../components/shared/Footer";
import Game from "../components/shared/game";
import Gamerhero from "../components/Main/gamer-hero";
import "../styles/game.css";
import { motion } from "framer-motion";
import { getGames } from "../services/gameService";
import Up from "../components/Main/Up";
import ScaleLoader from "react-spinners/ScaleLoader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import ChatMessage from "../components/shared/Chat-Message";
function Games() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const [isLoading, setIsLoading] = useState(true);
  const [games, setGames] = useState([]);
  const [isComplete, setComplete] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [pageNumbers, setPageNumbers] = useState([]);

  const token = localStorage.getItem("authToken");

  const findGames = async () => {
    setIsLoading(true);
    const gms = await getGames({ pageNumber }, { token });
    setGames(gms.data);
    console.log(gms.data);
    setTotalPages(gms.data.totalPages);
    setPageNumbers(
      Array.from({ length: gms.data.totalPages }, (_, index) => index + 1)
    );
    setComplete(true);
    setIsLoading(false);
  };
  useEffect(() => {
    findGames();
  }, [pageNumber]);

  const handlePageClick = (selectedPage) => {
    setPageNumber(selectedPage);
  };
  const handleNextPage = () => {
    setPageNumber(pageNumber + 1);
  };

  const handlePrevPage = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };
  const generatePageNumbers = () => {
    const currentPageGroup = Math.ceil(pageNumber / 10); // Obtener el grupo de página actual
    const startPage = (currentPageGroup - 1) * 10 + 1; // Calcular el número de inicio del grupo de página
    const endPage = Math.min(startPage + 9, totalPages); // Calcular el número de fin del grupo de página

    return Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);
  };
  return (
    <motion.div
      initial={{ width: 0 }}
      animate={{ width: "100%" }}
      exit={{ x: window.innerWidth }}
      transition={1}
    >
      <Navbar />
      <Gamerhero />
      {isLoading ? (
        <div className="loading-games">
          <ScaleLoader
            color="aqua"
            aria-label="Loading Spinner"
            style={{ position: "relative" }}
            height={210}
            width={48}
          />
        </div>
      ) : (
        <div className="all">
          <div className="the-game-container">
            {isComplete &&
              games.content.map((game) => {
                const screenshot = game.screenshot
                  ? game.screenshot
                  : "https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg";
                return (
                  <Game
                    image={screenshot}
                    title={game.gameDto.title}
                    description={game.gameDto.description}
                    creator={game.gameDto.steamId}
                    id={game.gameDto.id}
                    steamId={game.gameDto.steamId}
                    price="100"
                  />
                );
              })}
          </div>
          <div className="botones-juego">
      <button className="boton-juego" onClick={handlePrevPage} disabled={pageNumber === 1}>
        <FontAwesomeIcon icon={faArrowLeft} />
      </button>
      {generatePageNumbers().map((page) => (
        <button
          key={page}
          onClick={() => handlePageClick(page)}
          className="boton-juego"
          id={page === pageNumber ? 'boton-activo' : ''}
        >
          {page}
        </button>
      ))}
      <button className="boton-juego" onClick={handleNextPage} disabled={pageNumber === totalPages}>
        <FontAwesomeIcon icon={faArrowRight} />
      </button>
    </div>
        </div>
      )}
 {token && (
                    <ChatMessage/>

        )
        }
      <Up />
      <Footer />
    </motion.div>
  );
}

export default Games;
