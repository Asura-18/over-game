import React from 'react'
import  Navbar from '../components/shared/Navbar';
import  Footer from '../components/shared/Footer';
import '../styles/forms.css';
import Registerform from '../components/Main/Regsiter-form';
import {motion} from "framer-motion"

function Register() {
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}}>
        <Navbar/>
        <Registerform/>
      <Footer/>
    </motion.div>
  )
}

export default Register