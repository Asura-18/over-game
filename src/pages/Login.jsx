import React from 'react'
import {useEffect} from 'react'
import  Navbar from '../components/shared/Navbar';
import  Footer from '../components/shared/Footer';
import '../styles/forms.css';
import Login_form from '../components/Main/Login-form'
import {motion} from "framer-motion"

function Login() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}}>
        <Navbar/>
        <Login_form/>
      <Footer/>
    </motion.div>
  )
}

export default Login