import React, { useEffect } from 'react'
import AboutUsHero from '../components/AboutUs/AboutUsHero'
import AboutUsMain from '../components/AboutUs/AboutUsMain'
import OurTeam from '../components/AboutUs/OurTeam'
import Faq from '../components/AboutUs/Faq'
import Footer from '../components/shared/Footer'
import {motion} from "framer-motion"
import Navbar from '../components/shared/Navbar'
import Up from '../components/Main/Up'
import Copyright from '../components/AboutUs/Copyright'
import '../styles/AboutUs.css'
import ChatMessage from '../components/shared/Chat-Message'
function AboutUs() {
  const token = localStorage.getItem('authToken');

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}
  }
  transition={{duration:1}}
    >
        <Navbar />
        <AboutUsHero />
        <AboutUsMain /> 
        <Faq />
        <OurTeam />
        <Copyright/>
        {token && (
                    <ChatMessage/>

        )
        }
        <Up/>
        <Footer />
    </motion.div>
  )
}

export default AboutUs