import React from 'react'
import Navbar from '../components/shared/Navbar'
import Footer from '../components/shared/Footer'
import Settings from '../components/User/Settings'
import '../styles/Settings.css'

function SettingsPage() {
  return (
    <div>
        <Navbar></Navbar>
        <Settings></Settings>
        <Footer></Footer>
    </div>
  )
}

export default SettingsPage