import React from 'react'
import {useEffect} from 'react'
import  Navbar from '../components/shared/Navbar';
import  Footer from '../components/shared/Footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGhost } from '@fortawesome/free-solid-svg-icons';
import PacmanLoader    from "react-spinners/PacmanLoader";
import Errgame from '../components/Main/Err-game';
import '../styles/err-404.css'
import { useTranslation, Trans } from 'react-i18next';

function Err404() {
  const { t, i18n } = useTranslation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (

    <div className='error-404' >
        <Navbar/>
        <div className='error-container' style={{marginTop:'200px',marginBottom:'200px'}}>
        <PacmanLoader color="aqua" className='pacman-notfound' size={100}/>
            <h1>
                404 Oops!!
            </h1>
            <h3>Game over</h3>
            <p>
            {t('error.gameOverpar')}

            </p>
        </div>
        
        <Footer/>
    </div>
  )
}

export default Err404