import React,{useEffect} from 'react'
import Singlegame from '../components/shared/singlegame'
import Navbar from '../components/shared/Navbar'
import Footer from '../components/shared/Footer'
import { Fragment } from 'react'
import Up from '../components/Main/Up'
import ChatMessage from '../components/shared/Chat-Message'
function Game() {
  const token = localStorage.getItem('authToken');

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <Fragment>
        <Navbar/>
        <Singlegame/>
        {token && (
                    <ChatMessage/>

        )
        }
        <Up/>
        <Footer/>
    </Fragment>
  )
}

export default Game