import React, { Fragment } from 'react'
import Navbar from '../components/shared/Navbar'
import SearchGames from '../components/Main/SearchGames'
import Footer from '../components/shared/Footer'
import '../styles/Searchgame.css'
import Up from '../components/Main/Up';
import ChatMessage from '../components/shared/Chat-Message'
function SearchPage() {
  const token = localStorage.getItem('authToken');

  return (
    <Fragment>
        <Navbar/>
        <SearchGames/>
        {token && (
                    <ChatMessage/>

        )
        }
        <Up/>
        <Footer/>
    </Fragment>
  )
}

export default SearchPage