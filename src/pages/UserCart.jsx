import React,{useEffect} from 'react'
import '../styles/cart.css'
import Cart from '../components/Main/Cart'
import Navbar from '../components/shared/Navbar'
import Footer from '../components/shared/Footer'
import {motion} from "framer-motion"
function UserCart() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}}>
        <Navbar />
        <Cart/>
        <Footer />
    </motion.div>
  )
}

export default UserCart