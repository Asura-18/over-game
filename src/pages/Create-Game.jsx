import React,{useEffect} from 'react'
import CreateGameForm from '../components/User/CreateGameForm'
import { Fragment } from 'react'
import Navbar from '../components/shared/Navbar'
import Footer from '../components/shared/Footer'
import '../styles/CreateGameForm.css'
import {motion} from "framer-motion"

function CreateGame() {
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
  return (
   <motion.div
   initial={{width:0}}
   animate={{width:"100%"}}
   exit={{x:window.innerWidth}}>
    <Navbar />
    <CreateGameForm/>
    <Footer /> 
   </motion.div>
  )
}

export default CreateGame