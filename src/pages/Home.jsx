import React,{useState} from 'react'
import {useEffect} from 'react'
import  Navbar from '../components/shared/Navbar';
import  Footer from '../components/shared/Footer';
import  Hero from '../components/Main/Hero';
import  Main from '../components/Main/Main';
import  Platform from '../components/Main/Platform';
import  SpecialGames from '../components/Main/SpecialGames';
import Reservas from '../components/Main/Reservas';
import {motion} from "framer-motion"
import Masvendidos from '../components/Main/Masvendidos';
import Up from '../components/Main/Up';
import ChatMessage from '../components/shared/Chat-Message';
function Home() {
  const token = localStorage.getItem('authToken');
  const[id,setId]=useState("");

  
  useEffect(() => {
    window.scrollTo(0, 0);
    if(token){
      const dotIndex = token.indexOf('.');
        const dotIndex2 = token.indexOf('.', dotIndex + 1);
        const encodedSubstring = token.substring(dotIndex + 1, dotIndex2);
        const decodedToken = atob(encodedSubstring);
        const decodedObject = JSON.parse(decodedToken);
      setId(decodedObject.id) 
       }
  }, []);
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}
  }
  transition={{duration:1}}
    >
        <Navbar/>
        <Hero/>
        <SpecialGames id={id}/>
        <Platform/>
        <Reservas id={id}/>
        <Main/>
        <Masvendidos id={id}/>
        {token && (
                    <ChatMessage/>

        )
        }
        <Up/>
        <Footer/>
    </motion.div>
  )
}

export default Home