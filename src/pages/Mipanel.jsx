import React from 'react'
import {motion} from "framer-motion"

function Mipanel() {
  return (
    <motion.div
    initial={{width:0}}
    animate={{width:"100%"}}
    exit={{x:window.innerWidth}}>
      Mipanel
      </motion.div>
  )
}

export default Mipanel