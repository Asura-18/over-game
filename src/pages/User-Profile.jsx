import React, { useEffect, useState } from 'react';
import UserProfComp from '../components/User/UserProfComp';
import Navbar from '../components/shared/Navbar';
import Footer from '../components/shared/Footer';
import { motion } from 'framer-motion';
import '../styles/UserProf.css';
import { getUserDetails } from '../services/gameService';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ChatMessage from '../components/shared/Chat-Message';
function UserProfile() {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const token = localStorage.getItem('authToken');
  const dotIndex = token.indexOf('.');
  const dotIndex2 = token.indexOf('.', dotIndex + 1);
  const encodedSubstring = token.substring(dotIndex + 1, dotIndex2);
  const decodedToken = atob(encodedSubstring);
  const decodedObject = JSON.parse(decodedToken);
  const id = decodedObject.id;
  const role = decodedObject.role[0];
  const [userdetails, setUserdetails] = useState(null);
  const [profilePic,setProfilePic] = useState(null);


  const findUserDetails = async () => {
    const us_dets = await getUserDetails({ id }, { token });
    setUserdetails(us_dets.data);
  };

  useEffect(() => {
    findUserDetails();
    console.log(role);
  }, []);

  return (
    <motion.div
      initial={{ width: 0 }}
      animate={{ width: '100%' }}
      exit={{ x: window.innerWidth }}
    >
      <Navbar />
      {userdetails && (
        <UserProfComp
          name={userdetails.username}
          id={userdetails.id}
          friends={userdetails.friends.length}
          email={userdetails.email}
          description={userdetails.description}
          friendsRequest={userdetails.friendsRequest.length}
          favorites={userdetails.getGamesFavorites.length}
          role={
            role
          }
        />
      )}

      <Footer />
      {token && (
                    <ChatMessage/>

        )
        }
    </motion.div>
  );
}

export default UserProfile;
