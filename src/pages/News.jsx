import React, { useEffect, useState } from "react";
import Navbar from "../components/shared/Navbar";
import Footer from "../components/shared/Footer";
import NewsHero from "../components/News/NewsHero";
import RelNews from "../components/News/Relevant-news";
import NewsBanner from "../components/News/NewsBanner";
import "../styles/News.css";
import { motion } from "framer-motion";
import Up from "../components/Main/Up";
import { getGeneralNews } from "../services/gameService";
import ScaleLoader from "react-spinners/ScaleLoader";
import ChatMessage from "../components/shared/Chat-Message";
function News() {
  const token = localStorage.getItem('authToken');

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const [isLoading, setIsLoading] = useState(true);
  const [news, setNews] = useState([]);
  const findNews = async () => {
    setIsLoading(true);
    const nws = await getGeneralNews();
    setNews(nws.data);
    console.log(nws.data);
    setIsLoading(false);
  };
  useEffect(() => {
    findNews();
    console.log(news);
  }, []);
  return (
    <motion.div
      initial={{ width: 0 }}
      animate={{ width: "100%" }}
      exit={{ x: window.innerWidth }}
    >
      <Navbar />
      <NewsBanner />
      {isLoading ? (
        <div className="loading-games">
          <ScaleLoader
            color="aqua"
            aria-label="Loading Spinner"
            style={{ position: "relative" }}
            height={210}
            width={48}
          />
        </div>
      ) : (
        <div>
          <div style={{ marginTop: "130px", marginBottom: "100px" }}>
            <NewsHero news={news} />
          </div>
          <RelNews news={news} />
        </div>
      )}
       {token && (
                    <ChatMessage/>

        )
        }
      <Up />
      <Footer />
    </motion.div>
  );
}

export default News;
