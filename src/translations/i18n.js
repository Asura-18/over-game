import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Masvendidos from '../components/Main/Masvendidos';
i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, 
    },
    resources: {
      en: {
        translation: {
          aboutus: {
            hero1: 'Find Out About Us',
            hero2: 'Your Gaming Platform',
            textblk: 'About Us',
            textsubheading: 'Find a new way to enjoy games',
            textsubpar: 'Our goal is to provide a digital game distribution platform where users can discover, buy and play a wide variety of games for various platforms. We strive to provide an exceptional gaming experience and maintain an active community of enthusiastic players. Join us and discover a unique and exciting gaming experience.'
,            textsubteam: 'Find about our team'
,cophead: 'Copyright Disclaimer'
,coppar: 'The contents, texts, photographs, logos, images, computer programs, source codes and, in general, any existing intellectual creation in this Space, as a multimedia artistic work, are protected as copyright by the legislation on the subject of intellectual property.<br/> The company is not the owner of images, textures, graphics and any other multimedia content of the Web space or, in any case, it has the corresponding authorization for the use of said elements. The content provided in the Web Space may not be reproduced in whole or in part, nor transmitted, nor registered by any information retrieval system, in any form or by any means, unless prior authorization is obtained, in writing. , from the Valve organization'
,copvalve: '© All rights granted by Valve'
,faqtitle: 'Frequently Asked Questions'
,faqsubtitle: 'Let\'s answer your Questions'
,teamtitle: 'Our Team'
,teamcarlos:' Back end API developers are responsible for building and maintaining the backbone of our applications and systems. Using their experience in programming languages ​​such as Python, Java or Node.js, they are dedicated to designing and developing the APIs and business logic that allow our applications to work efficiently and securely. '
,teamaron: 'Our talented team of UI/UX developers is dedicated to creating exceptional and engaging user experiences. They work closely with designers and user experience specialists to design engaging and intuitive interfaces that make our products visually pleasing and easy to use.'
,teamfloren: 'Our Full Stack developers are experts in all layers of an application, both front end and back end. They are responsible for designing, developing, and maintaining entire applications, from the user interface to the database and server logic.'
,thanktitle: 'We Thank you for your support'
,thanksubheading: 'Dear team, today I am pleased to tell you that we have successfully reached the goal that we had set for ourselves, but all of this would not have been possible if we had not had your full support. In addition to my sincere thanks, I want to tell you that at the end of the month you will receive the promised bonus in your accounts. Once again thank you very much for putting so much effort and effort'

          },
          footer:{
             aboutcomptitle: 'About the Company',
             aboutcomppar: 'If your thing is video games and you love to spend hours hitting shots, visiting magical worlds, or driving rally cars: Overgame is your platform.',
            copyright: 'Company owned by Overgame © All rights Reserved',

            },
          navbar:{
             home: 'Home',
              games: 'Games',
          news: 'News',
          aboutus: 'About Us',
          placeholder: 'Search Games...',
            profile: 'Profile',
                    settings: 'Settings',
                    logout: 'Exit',
          },
          cart: {
            title: 'Game Deck',
            game: 'Games',
            gametitle: 'Title',
            details: 'Details',
            remove: 'Remove',
            sinjuegos: 'You don\'t have any games yet. Start adding them',

          },
          games: {
            herotitle: 'Games',
            herosubtitle: 'The Joy of Life',
            heropar: 'With nearly 30,000 games published; from major companies to independent studios and everything in between. Enjoy exclusive offers, automatic updates, and other great benefits.',
            explore: 'Explore',
            stores: 'Stores',
            news: 'News',
            nonews: 'No offers available',
            nostores: 'No news available'
          },
          home: {
            herotitle: 'Mission Mars',
            comprar: 'Buy',
            latestgame: 'Latest Games',
            latestgamesubtitle: 'Don\'t miss any offers or promotions!',
            latestgamepar: 'And be the first to receive our private offers, newsletters, and promotions of the week! Also, the European ESO Tavern community event is coming back with an exciting mix of activities and fun. You can now sign up for the celebration, which will take place in July 2023 at Satzvey Castle near the German city of Cologne! This year, it will be held at Satzvey Castle in Germany, between Saturday, July 15, at 15:00, and Sunday, July 16, at 14:00 (both in peninsular time).',
            Masvendidos: 'Best Sellers',
            Reservas: 'Pre-orders'
          },
          forms: {
            username: 'Username',
            password: 'Password',
            email: 'Email',
            login: 'Login',
            signup: 'Sign Up',
            newgametitle: 'Create a New Game',
            nombjuego: 'Game Name',
            steamid: 'Steam ID',
            description: 'Description',
            usersettings: 'User Settings',
            subir: 'Upload'
          },
          platform: {
            games: 'GAMES',
            news: 'NEWS'
          },
          news: {
            title: 'Never Stop',
            subtitle: 'Exploring the World',
            newspar: 'Log in to your Overgame account to access your personalized News Center or explore the featured news and updates below.',
            explore: 'Discover',
            proximamente: 'Upcoming',
            relevantnews: 'Relevant News'
          },
          chat: {
            friends: 'Friends',
            messages: 'Received Messages'
          },
          user: {
            user: 'User',
            fr: 'Friend Requests',
            nosol: 'You don\'t have any friend requests yet.',
            sinjuegos: 'No games',
            amigos: 'friends',
            requests: 'request',
            favoritos: 'favorites',
            mideck: 'My Deck',
            friends: 'Friends',
            nofriends: 'You don\'t have any friends yet, don\'t worry'
          },
          error: {
            gameOverpar: 'It seems that you have ventured into a secret area and have found our lost game page. Hold down the home button and come back soon for an epic gaming experience. There\'s no resume in real life, but there\'s always a next level in Overgame!',
          }
        }
      },
      es: {
        translation: {
          aboutus: {
            hero1: 'Descúbrenos más',
            hero2: 'Tu Plataforma de Juegos',
            textblk: 'Sobre Nosotros',
            textsubheading: 'Descubre una nueva forma de disfrutar de los juegos',
            textsubpar: 'Nuestro objetivo es proporcionar una plataforma de distribución digital de juegos,donde los usuarios pueden descubrir, comprar y jugar una amplia variedad de juegos para diversas plataformas. Nos esforzamos por ofrecer una experiencia de juego excepcional y mantener una comunidad activa de jugadores entusiastas. Únete a nosotros y descubre una experiencia de juego única y emocionante.'
            ,textsubteam: 'Descubre al equipo'
            ,cophead: 'Aviso de Copyright'
            ,coppar: ' Los contenidos, textos, fotografías, logotipos, imágenes, programas de ordenador, códigos fuente y, en general, cualquier creación intelectual existente en este Espacio, como obra artística multimedia, están  protegidos como derechos de autor por la legislación en materia de propiedad intelectual.<br/> La empresa no es titular de imágenes, texturas, gráficos y cualquier otro contenido multimedia del espacio Web o, en cualquier caso dispone de la correspondiente autorización para la utilización de dichos elementos. El contenido dispuesto en el Espacio Web no podrá    reproducido ni en todo ni en parte, ni transmitido, ni registrado por   ningún sistema de recuperación de información, en ninguna forma ni en  ningún medio, a menos que se cuente con la autorización previa, por escrito, de la organización de Valve.'
            ,copvalve: '© Derechos cedidos de Valve'
            ,faqtitle: 'Preguntas Frecuentes'
            ,faqsubtitle: 'Respondemos tus dudas'
            ,teamtitle: 'Nuestro equipo'
            ,teamcarlos: 'Los desarrolladores de Back end API son los encargados de construir y mantener la columna vertebral de nuestras aplicaciones y sistemas. Utilizando su experiencia en lenguajes de programación como Python, Java o Node.js, se dedican a diseñar y desarrollar las API y la lógica de negocio que permite que nuestras aplicaciones funcionen de manera eficiente y segura. '
            ,teamaron: 'Nuestro talentoso equipo de desarrolladores de UI / UX está dedicado a crear experiencias de usuario excepcionalesy atractivas. Trabajan en estrecha colaboración con diseñadores  y especialistas en experiencia del usuario para diseñar  interfaces intuitivas y atractivas que hagan que nuestros productos sean visualmente agradables y fáciles de usar.'
            ,teamfloren: 'Nuestros desarrolladores Full Stack son expertos en todas las capas de una aplicación, tanto en el front end como en el back end. Son responsables de diseñar, desarrollar y mantener aplicaciones completas, desde la interfaz de usuario hasta la base de datos y la lógica del servidor.'
            ,thanktitle: 'Agradecemos vuestro Apoyo'
            ,thanksubheading: 'Querido equipo hoy me complace decirles que hemos llegado con éxito a la meta que nos habíamos propuesto pero todo ello no hubiese sido posible si no hubiéramos contado con todo su apoyo.  Además de mi agradecimiento sincero quiero decirles que a fin de mes recibirán en sus cuentas el bono que se les había prometido. Una vez más muchas gracias por poner tanto empeño y esfuerzo  '

          },
          footer:{
             aboutcomptitle: 'Sobre la compañia',
              aboutcomppar: 'Si lo tuyo son los videojuegos y te encanta pasar horas pegando tiros, visitando mundos mágicos, o conduciendo coches de rally: Overgame es tu plataforma.',
          copyright: 'Empresa por Overgame © Todos los derechos reservados',

          },
          navbar:{
            home: 'Inicio',
          games: 'Juegos',
          news: 'Noticias',
          aboutus: 'Sobre Nosotros',
                    placeholder: 'Buscar Juegos...',
                    profile: 'Perfil',
                    settings: 'Settings',
                    logout: 'Salir',



          },
          cart:{
            title: 'Game Deck',
          game: 'Juegos',
          gametitle: 'Titulo',
          details: 'Detalles',
          remove: 'Eliminar',
          sinjuegos: ' No tienes juegos en el GameDeck, Empieza añadiendo ya.',
          },
          games:{
            herotitle: 'Juegos',
          herosubtitle: 'La alegría de la vida',
          heropar: 'Con casi 30 000 juegos publicados; desde grandes compañías hasta estudios independientes pasando portodo lo intermedio. Disfruta de ofertas exclusivas, actualizaciones automáticas y otras grandes ventajas.',
          explore: 'Explorar',
          stores: 'Tiendas',
          news: 'Noticias',
          nonews: 'No hay ofertas disponibles',
          nostores: 'No hay noticias disponibles',
          },
          home:{
          herotitle: 'Misión Marte',
          comprar: 'Comprar',
          latestgame: 'Últimos Juegos',
          latestgamesubtitle: '¡No te pierdas ninguna oferta o promoción!',
          latestgamepar: '¡Y sé el primero en recibir nuestras ofertas privadas, newsletters y promociones de la semana! Además, vuelve el evento de la comunidad europea ESO Tavern con un emocionante cóctel de actividades y diversión. ¡Ya podéis apuntaros para la celebración, que tendrá lugar en julio de 2023 en el castillo de Satzvey, cerca de la ciudad alemana de Colonia! Este año se celebrará en el castillo de Satzvey en Alemania, entre el sábado, 15 de julio, a las 15:00, y el domingo, 16 de julio, a las 14:00 (ambos en horario peninsular).'
          ,Masvendidos: 'Más Vendidos'
          ,Reservas: 'Reservas'
          },
          forms:{
          username: 'Nombre de Usuario',
          password: 'Contraseña',
          email: 'Correo',
          login: 'Iniciar Sesión',
          signup: 'Registrar',
          newgametitle: 'Crear un nuevo juego',
          nombjuego: 'Nombre del Juego',
          steamid: 'Id de Steam',
          description: 'Descripción ',
          usersettings: 'Configuración ',
          subir: 'Subir ',
        }
          ,
          platform:{
          games: 'JUEGOS',
          news: 'NOTICIAS',
          }
          ,
          news:{
            title: 'Nunca Pares',
            subtitle: 'De Explorar el Mundo',
            newspar: 'Inicia sesión con tu cuenta de Overgame para acceder a tu Centro de noticias personalizado o explora las noticias y actualizaciones destacadas más abajo..',
            explore: 'Descubre',
            proximamente: 'Próximamente',
            relevantnews: 'Noticias Relevantes',
          },
          chat:{
            friends: 'Amigos',
            messages: 'Mensajes Recibidos',
          }
          ,
          user:{
            user: 'Usuario',
            fr: 'Solicitudes de Amistad',
            nosol: 'No tienes solicitudes de amistad todavía.',
            sinjuegos: 'No hay juegos',
            amigos: 'amigos',
            requests: 'solicitud',
            favoritos: 'favs',
            mideck: 'Mi Deck',
            friends: 'Amigos',
            nofriends: 'No tienes amigos todavía, no te preocupes',
          },
          error: {
            gameOverpar: 'Parece que te has aventurado en una zona secreta y has encontrado nuestra página de juegos perdida. Mantén pulsado el botón de inicio y regresa pronto para disfrutar de una experiencia de juego épica. ¡No se puede reanudar en la vida real, pero siempre hay un próximo nivel en Overgame!',
          }

        }
      }
    }
  });
export default i18n;