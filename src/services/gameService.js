import axios from "axios";

const BASE_URL = "http://localhost:8080";

export const findCurrentUser = async ({token}) => {
  try {
      //console.log("serviceeee"+token);
      const response = await axios.get(BASE_URL + "/admin/rest/currentUser" , {
          headers: {
              'Authorization': 'Bearer ' + token
          }
      });
      return response;
  } catch (error) {
      // Handle the error
  }
};


export const getGames = async ({ pageNumber }, { token }) => {
    try {

      const response = await axios.get(BASE_URL + "/?pageNumber=" + pageNumber, {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
      return response;
    } catch (error) {
            // Handle the error
            console.log(error);
    }
  };
  export const getAllGames = async () => {
    try {

      const response = await axios.get(BASE_URL + "/");
      return response;
    } catch (error) {
            // Handle the error
            console.log(error);
    }
  };
  export const getGameStores = async ({ steamId }) => {
    try {
      console.log(steamId);
      const response = await axios.get(BASE_URL + "/game/price/" + steamId);
      return response;
    } catch (error) {
      // Manejar el error
      console.log(error);
    }
  };
  export const gameDetailsSteamWebApi = async ({steamId}) => {
    try {
        const response = await axios.get(BASE_URL+"/game/steam/details/"+steamId);
        return response;
        // return await axios.get(BASE_URL + "/game/steam/details/" + steamId);
    } catch (e) {
        console.error(e);
    }
    // return null;
}
export const gameNewsSteamWebApi = async ({steamId}) => {
  try {
    const response = await axios.get(BASE_URL + "/game/steam/news/" + steamId);
      return response;
  } catch (e) {
      console.error(e);
  }
  return null;
}

export const findGameByTitle = async ({title}) => {
    try {
        return await axios.get(BASE_URL + "/game?title=" + title);
    } catch (e) {
        console.error(e);
    }
    return null;
}
// export const findProfilePic = async ({token}) => {
//     try {
//          await axios.get(BASE_URL + "/user/picture" , {
//         headers: {
//           'Authorization': 'Bearer ' + token
//         }
//       });
//     } catch (e) {
//         console.error(e);
//     }
//     return null;
// }
export const getPfp = async ({token}) => {
  try {
      const response = await axios.get(BASE_URL + "/user/picture", {
          responseType: 'arraybuffer',
          headers: {
              'Authorization': 'Bearer ' + token
          }
      });
      // Convert the image data to a base64 string
      const imageBase64 = btoa(
          new Uint8Array(response.data)
              .reduce((data, byte) => data + String.fromCharCode(byte), '')
      );
      // Create the data URL for displaying the image
      return ('data:' + response.headers['content-type'] + ';base64,' + imageBase64);
  } catch (error) {
      // Handle the error
      console.log(error);
  }
};

export const setPfp = async ({formData}, {token}) => {
  try {
      console.log(formData.get('profilePic'));
      const response = await axios.post(BASE_URL + "/user/picture", formData, {
          headers: {
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'multipart/form-data',
          }
      });
      return response;
  } catch (error) {
      // Handle the error
      console.log(error);
  }
};

export const getGameDetailsById = async ({id}) => {
    try {
        return await axios.get(BASE_URL + "/game/" + id);
    } catch (e) {
        console.error(e);
    }
    return null;
}

export const saveGame = async ({title, description, steamId}) => {
    try {
        return await axios.post(BASE_URL + "/game/", {
            title,
            description,
            steamId
        });
    } catch (e) {
        console.error(e);
    }
    return null;
}

export const deleteGameById = async ({id}) => {
    try {
        return await axios.delete(BASE_URL + "/game/delete/" + id);
    } catch (e) {
        console.error(e);
    }
    return null;
}

export const getDetailsUpdateGame = async ({id}) => {
    try {
        return await axios.get(BASE_URL + "/game/edit/" + id);
    } catch (e) {
        console.error(e);
    }
    return null;
    
}
/**get General News */
export const getGeneralNews = async () => {
  try {
    const response = await axios.get(BASE_URL + "/game/steam/news");
      return response;
  } catch (e) {
      console.error(e);
  }
  return null;
  
}

export const updateGame = async ({id, title, description, steamId}) => {
    try {
        return await axios.put(BASE_URL + "/game/edit/", {
            id,
            title,
            description,
            steamId
        });
    } catch (e) {
        console.error(e);
    }
    return null;
}





export const getUserFavourites = async ( { token }) => {
    try {

      const response = await axios.get(BASE_URL + "/game/favorites" , {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
      return response;
    } catch (error) {
            // Handle the error
            console.log(error);
    }
    
}
export const deleteUserFavorites = async ( {id},{ token }) => {
    try {
        console.log(id);
        console.log(token);
      const response = await axios.delete(BASE_URL + "/game/delete/favorite/"+id, {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
      console.log(response);

      return response;
    } catch (error) {
            // Handle the error
            console.log(error);
    }
    
}
export const insertUserFavorites = async ({ id }, { token }) => {
    try {
      console.log(id);
      console.log(token);
      const response = await axios.post(
        BASE_URL + "/game/favorite/" + id,
        {},
        {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        }
      );
      console.log(response);
      return response;
    } catch (error) {
      // Handle the error
      console.log(error);
    }
  }

/***************User panel métodos *************************/
export const getUserDetails = async ({ id }, { token }) => {
  try {
    const response = await axios.get(
      BASE_URL + "/admin/rest/user/" + id,
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
}

/***************User panel amigos métodos *************************/
export const getFriends = async ({ token }) => {
  try {
    const response = await axios.get(
      BASE_URL + "/friends",
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
}

export const getFrRequests = async ({ token }) => {
  try {
    const response = await axios.get(
      BASE_URL + "/friends/request",
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
}

export const friendConfirm = async ({ token },{id}) => {
  try {
    const response = await axios.post(
      BASE_URL + "/friend/confirm/"+id, {},
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    console.log(response);
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
}

export const friendAdd = async ({ token },{id}) => {
  try {
    console.log('gs:'+ token,id);
    const response = await axios.post(
      BASE_URL + "/friend/add/"+id, {},
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    console.log(response);
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
}

export const friendDecline = async ({ token }, { id }) => {
  try {
    const response = await axios.delete(BASE_URL + "/friend/decline/" + id, {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    return response;
  } catch (error) {
    // Manejar el error
    console.log(error);
  }
};

export const friendRemove = async ({ token }, { id }) => {
  try {
    const response = await axios.delete(BASE_URL + "/friend/remove/" + id, {
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    return response;
  } catch (error) {
    // Manejar el error
    console.log(error);
  }
};
/**ADMIN-->  Creación de juegos */
export const createGame = async ({ token }) => {
  try {
    const response = await axios.post(
      BASE_URL + "/game/new", {

      },
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }
    );
    console.log(response);
    return response;
  } catch (error) {
    // Handle the error
    console.log(error);
  }
};
