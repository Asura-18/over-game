import React from 'react'
import '../../styles/Platform.css'
import { useTranslation, Trans } from 'react-i18next';

function Platform() {
  const { t, i18n } = useTranslation();

  return (
    <div className='platform-container'>
        <div className="background-one">
            <div className="link-container">
                <a className="link-one" target="_blank" rel="noopener noreferrer" href='/games'>{t('platform.games')}</a>
            </div>
            </div>
            <div className="background-three link-container">
            <a className="link-three" target="_blank" rel="noopener noreferrer" href='/news'>{t('platform.news')}</a>
            </div>
    </div>
  )
}

export default Platform