import React, { useState, useEffect } from "react";
import Game from "../shared/game";
import axios from "axios";
import { useLocation } from "react-router-dom";
import ScaleLoader from "react-spinners/ScaleLoader";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import sad from '../../assets/img/main/sad.png';

function SearchGames() {
  const location = useLocation();
  const [searchTerm, setSearchTerm] = useState("");
  const [games, setGames] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [pageNumbers, setPageNumbers] = useState([]);
  const handlePageClick = (selectedPage) => {
    setPageNumber(selectedPage);
  };
  const handleNextPage = () => {
    setPageNumber(pageNumber + 1);
  };

  const handlePrevPage = () => {
    if (pageNumber > 1) {
      setPageNumber(pageNumber - 1);
    }
  };
  const generatePageNumbers = () => {
    const currentPageGroup = Math.ceil(pageNumber / 10); // Obtener el grupo de página actual
    const startPage = (currentPageGroup - 1) * 10 + 1; // Calcular el número de inicio del grupo de página
    const endPage = Math.min(startPage + 9, totalPages); // Calcular el número de fin del grupo de página
    console.log(Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index));
    return Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);
  };
  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const title = searchParams.get("title");

    setSearchTerm(title || "");

    searchGames(title || "");
    
  }, [pageNumber]);

  const searchGames = async (query) => {
    try {
      setIsLoading(true);
      const response = await axios.get(
        `http://localhost:8080/game?title=${query}&pageNumber=${pageNumber}`
      );
      console.log(response.data.totalPages);
      const gameData = response.data.content;
      setGames(gameData);
      setTotalPages(response.data.totalPages); // Corregir la asignación de totalPages
      setPageNumbers(
        Array.from({ length: gameData.totalPages }, (_, index) => index + 1)
      );
  
    
      setIsLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSearchInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    searchGames(searchTerm);
  };
  return (
    <div className="game-search">
      <div className="search-container">
        <form onSubmit={handleSearchSubmit} >
        <div class="que-Card">
              <div class="CardInner">
              <label>Search for your favourite games</label>
              <div class="container">
                <div class="InputContainer">
                  <input type="text" value={searchTerm}
            onChange={handleSearchInputChange}/>
                </div>
                <div class="Icon">
                 <button  type="submit"  >      <FontAwesomeIcon icon={faMagnifyingGlass} />
</button> 
                </div>
              </div>
            </div>
            </div>
          {/* <button type="submit" className="search-btn"><FontAwesomeIcon icon={faMagnifyingGlass}/></button> */}
        </form>
        <div className="game-list-found">
          {isLoading ? (
            <div className="loading-games">
              <ScaleLoader
                color="aqua"
                aria-label="Loading Spinner"
                style={{ position: "relative" }}
                height={210}
                width={48}
              />
            </div>
          ) : games.length > 0 ? (
            <div className="all">
               <div className="the-game-container">
              {games.map((game) => (
                <Game key={game.gameDto.id} game={game}
                image={game.screenshot ? game.screenshot : "https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg"} title={game.gameDto.title} description={game.gameDto.description} creator={game.gameDto.steamId} id={game.gameDto.id} steamId={game.gameDto.steamId} price="100"
                 />
              ))}
            </div>
            <div className="botones-juego">
      <button className="boton-juego" onClick={handlePrevPage} disabled={pageNumber === 1}>
        <FontAwesomeIcon icon={faArrowLeft} />
      </button>
      {generatePageNumbers().map((page) => (
        <button
          key={page}
          onClick={() => handlePageClick(page)}
          className="boton-juego"
          id={page === pageNumber ? 'boton-activo' : ''}
        >
          {page}
        </button>
      ))}
      <button className="boton-juego" onClick={handleNextPage} disabled={pageNumber === totalPages}>
        <FontAwesomeIcon icon={faArrowRight} />
      </button>
    </div>
            </div>
           
          ) : (
            <div className="no-games-found">
            <p>No se encontraron juegos.</p>
            <img src={sad} alt="sad.png" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default SearchGames;
