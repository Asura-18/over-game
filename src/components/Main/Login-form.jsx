import React,{useState,useEffect} from 'react'
import axios from 'axios';
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useTranslation, Trans } from 'react-i18next';

function Loginform() {
    const navigate = useNavigate();
    const { t, i18n } = useTranslation();

   
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [showModal, setShowModal] = useState(false);

    const login = async (e) => {
      e.preventDefault();
      try {
        const response = await axios.post('http://localhost:8080/api/v1/auth/login', {
          username: username,
          password: password
        });
        const authToken = response.data.token;
        localStorage.setItem('authToken', authToken);
        console.log(JSON.stringify(response.data.token));
        navigate('/profile');
      } catch (error) {
        console.log(error);
        toast.error('Error al inciar sesión', {
          position: "top-right",
          autoClose: 12000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
        });
        setShowModal(true);
      }
  };
  return (
    <div className='Forms'>
        <div className="loginForm">
            <div className="login-box">
                
                    <div className="textbox">
                            <div className="log-form">               
                             <h2>{t('forms.login')} </h2>
                                <form onSubmit={login}>
                                <div className="input-box">
                                    <input type="text" required name='username'  value={username}
          onChange={(e) => setUsername(e.target.value)}/>
                                    <label htmlFor="username">{t('forms.username')}</label>
                                    <i></i>
                                </div>
                                <div className="input-box">
                                    <input type="password" required name='password' value={password}
          onChange={(e) => setPassword(e.target.value)}/>
                                    <label htmlFor="password">{t('forms.password')}</label>
                                    <i></i>
                                </div>
                               
                               <input type="submit" value={'Login'}/>
                               <div className="links">
                                    <NavLink to={"/register"}>{t('forms.signup')}</NavLink>
                               </div>
                                </form>
                            </div>
                    </div>
                
            </div>
        </div>
        {showModal && (
        <ToastContainer
        position="top-right"
        autoClose={12000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        />
      )}
    </div>
  )
}

export default Loginform