import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark} from '@fortawesome/free-solid-svg-icons'

function Modal({ error, titulo, close }) {
  return (
    <div className="popup-overlay-error">
        <div className='popup-content-error' style={{color:'black'}}>
           <h1>{titulo}</h1>
        <p>{error}</p>
        <button onClick={close}><FontAwesomeIcon icon={faCircleXmark} /></button> 
        </div>
        
    </div>
  )
}

export default Modal