import React, { useEffect, useState } from "react";
import prodimg from '../../assets/img/favico/buff.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark} from '@fortawesome/free-solid-svg-icons'
import {faArrowUpRightFromSquare} from '@fortawesome/free-solid-svg-icons'
import { getUserFavourites } from "../../services/gameService";
import {  getGameDetailsById } from '../../services/gameService';
import { NavLink } from "react-router-dom";
import { deleteUserFavorites } from '../../services/gameService';
import { useTranslation, Trans } from 'react-i18next';

function Cart() {
  // const [quantity, setQuantity] = useState(1);
  const { t, i18n } = useTranslation();


  // const handleDecrement = () => {
  //   if (quantity > 1) {
  //     setQuantity(quantity - 1);
  //   }
  // };

  // const handleIncrement = () => {
  //   setQuantity(quantity + 1);
  //   if(quantity>=10){
  //       setQuantity(10);
  //   }
  // };
  const [favourites, setFavourites] = useState([]);
  const [gameDetails, setGameDetails] = useState([]);
  
  
  const token = localStorage.getItem('authToken');
  
  
  const handleDeleteFavorite = async (id) => {
    try {
      await deleteUserFavorites({id},{token});
      await findFavorites();
    } catch (error) {
      console.error(error);
    }
  };
  const findFavorites = async () => {
    const favs = await getUserFavourites({ token });
    setFavourites(favs.data);
  };
  const findGameDetails = async () => {
    const gameDetailsPromises = favourites.map(async (id) => {
      const response = await getGameDetailsById({ id });
      return response.data;
    });
  
    const gameDetails = await Promise.all(gameDetailsPromises);
    setGameDetails(gameDetails);

  };
  
  useEffect(() => {
    findFavorites();
  }, []);
  
  useEffect(() => {
    if (favourites.length > -1) {
      findGameDetails();
    }
  }, [favourites]);

  return (
    <div className="game-deck">
        <div className="cart-title">
            <h1>{t('cart.title')}</h1>
        </div>
      <div className="small-container cart-page">
        
        <table>
          <thead>
            <tr>
              <th>{t('cart.game')}</th>
              <th>{t('cart.gametitle')}</th>
              {/* <th>Quantity</th> */}
              <th>{t('cart.details')}</th>
              <th>{t('cart.remove')}</th>
            </tr>
          </thead>
          <tbody>
 {gameDetails.length > 0 ? (
  gameDetails.map((game) => (
    <tr key={game.gameDto.id}>
      <td>
        <div className="cart-info">
          <img src={game.screenshot ? game.screenshot : "https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg"} alt="prod-img" />
        </div>
      </td>
      <td>
        <div className="details">
          <p>{game.gameDto.title}</p>
        </div>
      </td>
      <td  className="link-a-juego">
        <NavLink to={`/games/${game.gameDto.id}/${game.gameDto.steamId}`} target="_blank">
          <FontAwesomeIcon icon={faArrowUpRightFromSquare} />
        </NavLink>
      </td>
      <td className="remove">
        <a onClick={() => handleDeleteFavorite(game.gameDto.id)}>
          <FontAwesomeIcon icon={faCircleXmark} />
        </a>
      </td>
    </tr>
  ))
) : (
  <tr className="no-juegos">
    <td colSpan="4">
      <p>{t('cart.sinjuegos')}</p>
    </td>
  </tr>
)}
          </tbody>
        </table>
        
      </div>
    </div>
  );
}

export default Cart;
