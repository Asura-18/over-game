import React, { Fragment, useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../../styles/SpecialGames.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getAllGames } from '../../services/gameService';
import ScaleLoader from 'react-spinners/ScaleLoader';
import { insertUserFavorites } from "../../services/gameService";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
// import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
// import { Carousel } from 'react-responsive-carousel';
import Game from '../shared/game';

function SpecialGames(props) {
  const [juegos, setJuegos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [slider, setSlider] = useState(false);
  const defaultImage = 'https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg'; // URL de la imagen alternativa
const token = localStorage.getItem('authToken');
  const insertFav = async (id) => {
  try {
    await insertUserFavorites({id},{token});
  } catch (error) {
    console.error(error);
  }
};
  const cogerjuegos = async () => {
    try {
      const response = await getAllGames();
      setJuegos(response.data.content.slice(1,9));
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  //npm carrousel 
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1500 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1500, min: 1026 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 1026, min: 0 },
      items: 1
    }
  };



  //npm 
  useEffect(() => {
    cogerjuegos();
    const divElement = document.querySelector('.react-multi-carousel-list');
    if (divElement) {
      divElement.id = 'carousell';
    }
  }, []);

  if (loading) {
    return (
    <div className="loading-games">
        <ScaleLoader  color="aqua" aria-label="Loading Spinner" style={{position:'relative'}} height={210} width={48} />
      </div> 
    )
     // Vista de carga mientras se obtienen los datos
  }

  return (
    <Fragment>
      <div className="special-game">
        <Carousel 
        responsive={responsive}
        >
         { juegos && juegos.map((game) => (
            <Game title={game.gameDto.title} id={game.gameDto.id} image={game.screenshot !== null ? game.screenshot : defaultImage} steamId={game.gameDto.steamId}
            user_id={game.gameDto.id}
          />
          ))}
        </Carousel>
      </div>
    </Fragment>
  );
}

export default SpecialGames;
