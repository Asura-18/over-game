import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleXmark} from '@fortawesome/free-solid-svg-icons'


function Errgame(props) {
  return (
    <div style={{width:'100%',height:'100%',position:'absolute',zIndex:'3',marginTop:'-950px',paddingTop:'400px',
    background: 'rgba(0, 0, 0, .65) url(´http://fc02.deviantart.net/fs71/i/2011/274/6/f/ocean__sky__stars__and_you_by_muddymelly-d4bg1ub.png´)',
    backgroundBlendMode: 'darken'}}>
      <div style={{ margin: "0 auto", maxWidth:'500px', }}>
        <iframe
          style={{ width: "100%", height: "550px", overflow: "hidden",border:'none'}}
          src="https://playpager.com/embed/checkers/index.html" title="err-game"
          scrolling="no"
        ></iframe>
        <button onClick={props.close} style={{position:'absolute',backgroundColor:'transparent',cursor:'pointer',fontSize:'50px',padding:'10px',color:'red',border:'none'}}><FontAwesomeIcon icon={faCircleXmark} /></button>
      </div>
    </div>
  );
}

export default Errgame;
