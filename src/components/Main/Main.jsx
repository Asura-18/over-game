import React from "react";
import { Carousel } from "react-carousel-minimal";
import g1 from '../../assets/img/slider/g1.jpg'
import g2 from '../../assets/img/slider/g2.jpg'
import g3 from '../../assets/img/slider/g3.jpg'
import g4 from '../../assets/img/slider/g4.jpg'
import g5 from '../../assets/img/slider/g5.jpg'
import g6 from '../../assets/img/slider/g7.jpg'
import '../../styles/Main.css'
import { useTranslation, Trans } from 'react-i18next';

function Main() {
  const { t, i18n } = useTranslation();

  const data = [
    {
      image:
      g2 ,
      caption: "Fornite Crew",
    },{
      image:g6 ,
      caption: "God of War",
    },
    {
      image:
      g3 ,
      caption: "Last of Us",
    },
    {
      image:
      g4 ,
      caption: "Dark Realm 2",
    },
    {
      image:
      g5 ,
      caption: "Frontier Battles",
    },
    {
      image:
      g1 ,
      caption: "Rainbow Saga",
    }
  ];

  const captionStyle = {
    fontSize: "3.5em",
    textAlign:'center',
    fontWeight: "bold",
  };
  const slideNumberStyle = {
    fontSize: "20px",
    fontWeight: "bold",
  };
  return (
    <div>
      <div className="slider">
      <h1>{t('home.latestgame')}</h1>
        <div className="slider-content">
            <Carousel
              data={data}
              time={4000}
              width="700px"
              height="450px"
              captionStyle={captionStyle}
              radius="10px"
              slideNumber={false}
              slideNumberStyle={slideNumberStyle}
              captionPosition="bottom"
              automatic={true}
              dots={false}
              className="carousel-main"
              pauseIconColor="white"
              pauseIconSize="40px"
              slideBackgroundColor="darkgrey"
              slideImageFit="cover"
              thumbnails={false}
              thumbnailWidth="100px"
              style={{
                textAlign: "center",
                maxwidth:'500px'
              }}
            />
            <div className="slide-content">
              <h3>              {t('home.latestgamesubtitle')}
</h3>
              <p>
              {t('home.latestgamepar')}
              </p>
            </div>
        </div>
        </div>
    </div>
  );
}

export default Main;
