import React from 'react'
import { useTranslation, Trans } from 'react-i18next';

function Gamerhero() {
  const { t, i18n } = useTranslation();

  return (
<div className='gamer-hero'>
<section className="showcase-game">
    <div className="overlay"></div>
    <div className="text">
      <h2>{t('games.herotitle')} </h2> 
      <h3>{t('games.herosubtitle')}</h3>
      <p>{t('games.heropar')}</p>
      <a href="#">{t('games.explore')}</a>
    </div>
  </section>
    </div>
  )
}

export default Gamerhero