import React, { Fragment,useState,useEffect } from "react";
import "../../styles/Reservas.css";
import axios from 'axios'
import Game_card_home from "../shared/game_card_home";
import Game from "../shared/game";
import { getAllGames } from '../../services/gameService';
import ScaleLoader from 'react-spinners/ScaleLoader';
import { useTranslation, Trans } from 'react-i18next';

function Reservas(props) {
   // llamada a la API
  const [juegos, setJuegos] = useState([]);
  const [loading, setLoading] = useState(true);
  const [slider, setSlider] = useState(false);
  const defaultImage = 'https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg'; // URL de la imagen alternativa
  const { t, i18n } = useTranslation();

  const cogerjuegos = async () => {
    try {
      const response = await getAllGames();
      setJuegos(response.data.content.slice(1,10));
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    cogerjuegos();
  }, []);

  if (loading) {
    return (
    <div className="loading-games">
        <ScaleLoader  color="aqua" aria-label="Loading Spinner" style={{position:'relative'}} height={210} width={48} />
      </div> 
    )
  }
  return (
    <div className="cards_all">
      <h2>{t('home.Reservas')}</h2>
      <div className="cards">
      {
          juegos && juegos.map((game)=>{
            return(
            <Game title={game.gameDto.title} id={game.gameDto.id} image={game.screenshot !== null ? game.screenshot : defaultImage} steamId={game.gameDto.steamId}
            user_id={game.gameDto.id}
          />
           ) })
        }
      </div>
    </div>
  );
}

export default Reservas;
