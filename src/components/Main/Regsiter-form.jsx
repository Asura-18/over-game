import React ,{useState}from 'react'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Modal from './Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram} from '@fortawesome/free-brands-svg-icons'
import { faFacebook} from '@fortawesome/free-brands-svg-icons'
import { faTwitter} from '@fortawesome/free-brands-svg-icons'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useTranslation, Trans } from 'react-i18next';


function Registerform() {
  const { t, i18n } = useTranslation();

    const navigate = useNavigate();
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [showModal, setShowModal] = useState(false);
    const login = async (e) => {
        e.preventDefault();
    try {
      const response = await axios.post('http://localhost:8080/api/v1/auth/register', {
        username: username,
        password: password,
        email:email
      });
      const authToken = response.data.token;
      localStorage.setItem('authToken',authToken);
      console.log(JSON.stringify(response.data.token));
      navigate('/profile');

    } catch (error) {
      console.log(error);
      toast.error('Error al registrarse', {
        position: "top-right",
        autoClose: 12000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
      setShowModal(true); 
    }
  };
//   const closeModal = () => {
//     setShowModal(false); 
//   };
  return (
    <div className='Forms'>
        <div className="registerForm">
            <div className="register-box">
                
                    <div className="textbox">
                            <div className="log-form">               
                             <h2>{t('forms.signup')}</h2>
                                <form onSubmit={login}>
                                <div className="input-box">
                                    <input type="text"  value={username} required name='username' onChange={(e) => setUsername(e.target.value)}/>
                                    <label htmlFor="username">{t('forms.signup')}</label>
                                    <i></i>
                                </div>
                                <div className="input-box">
                                    <input type="email" pattern='^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$' name='email' value={email} required onChange={(e) => setEmail(e.target.value)}/>
                                    <label htmlFor="email">{t('forms.email')}</label>
                                    <i></i>
                                </div>
                                <div className="input-box">
                                    <input type="password" required name='password' value={password}
          onChange={(e) => setPassword(e.target.value)}/>
                                    <label htmlFor="password">{t('forms.password')}</label>
                                    <i></i>
                                </div>
                               
                               <input type="submit" value={'Sign up'}/>
                               <div className="links">
                               <a href="/">
                                <FontAwesomeIcon icon={faInstagram} style={{backgroundColor:'transparent'}}/>            
                                </a>
                                <a href="/">
                                <FontAwesomeIcon icon={faFacebook} />            
                                </a>
                                <a href="/">
                                <FontAwesomeIcon icon={faTwitter} />            
                                </a>
                               </div>
                            </form>
                            </div>
                    </div>
                
            </div>
        </div>
        {showModal && (
        <ToastContainer
        position="top-right"
        autoClose={12000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        />
      )}
    </div>
  )
}

export default Registerform;