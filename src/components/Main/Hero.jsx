import React from "react";
import '../../styles/Hero.css';
import hero_banner2 from '../../assets/img/main/halo.png';
import { Link } from 'react-router-dom';
import { useTranslation, Trans } from 'react-i18next';

function Hero() {
  const { t, i18n } = useTranslation();

  return (
    <div className="main">    
      <div className="overlay"></div>
      {/* <img src={hero_banner} alt="" /> */}
      <div className="hero-image">
      </div>
      <div className="content">
        <div className="cont-p">
           <h1>{t('home.herotitle')}</h1>
        <h4>6,99€</h4>
        <button><h3><Link to={'https://store.steampowered.com/app/1576620/Mission_Mars/'} target="_blank">{t('home.comprar')}</Link></h3></button>
        </div>
       
      </div>
      <div className="separator"></div>
    </div>
    
  );
}

export default Hero;
