import React, { Fragment } from "react";
import imagen from '../../assets/img/other/cool-geometric-triangular-figure-neon-laser-light-great-backgrounds-wallpapers.jpg'
import { useTranslation, Trans } from 'react-i18next';

function NewsHero(props) {
  const { t, i18n } = useTranslation();

  console.log(props);
  return (
    <Fragment>
      <div className="news-hero-band">
        <h2>{t('news.proximamente')}</h2>
        <div style={{ clear: 'both',backgroundColor:'grey',height:'2px',marginTop:'22px',marginLeft:'-80px'}}></div>
        <div className="news-hero-item-1">
          <a
            href={props.news[0].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[0].screenshotUrl})`}}
            ></div>
            <article>
              <h1>{props.news[0].gameSteamNewsData.title}</h1>
              <span>{props.news[0].gameSteamNewsData.author ? props.news[0].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div className="news-hero-item-2">
          <a
            href={props.news[1].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[1].screenshotUrl})`}}

            ></div>
            <article>
              <h1>{props.news[1].gameSteamNewsData.title}</h1>
              <p>
              </p>
              <span>{props.news[1].gameSteamNewsData.author ? props.news[1].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div classNameName="news-hero-item-3">
          <a
            href={props.news[2].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[2].screenshotUrl})`}}

            ></div>
            <article>
              <h1>{props.news[2].gameSteamNewsData.title}</h1>
           
              <span>{props.news[2].gameSteamNewsData.author ? props.news[2].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div className="news-hero-item-4">
          <a
            href={props.news[3].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[3].screenshotUrl})`}}

            ></div>
            <article>
              <h1>{props.news[3].gameSteamNewsData.title}</h1>
              <p>

              </p>
              <span>{props.news[3].gameSteamNewsData.author ? props.news[3].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div className="news-hero-item-5">
          <a
            href={props.news[4].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[4].screenshotUrl})`}}

            ></div>
            <article>
              <h1>
              {props.news[4].gameSteamNewsData.title}
              </h1>
              <span>{props.news[4].gameSteamNewsData.author ? props.news[4].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div className="news-hero-item-6">
          <a
            href={props.news[5].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[5].screenshotUrl})`}}

            ></div>
            <article>
              <h1>
              {props.news[5].gameSteamNewsData.title}              
              </h1>

              <span>{props.news[5].gameSteamNewsData.author ? props.news[5].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
        <div className="news-hero-item-7">
          <a
            href={props.news[6].gameSteamNewsData.url}
            className="news-hero-card" target="_blank"
          >
            <div
              className="news-hero-thumb"
              style={{ backgroundImage:`url(${props.news[6].screenshotUrl})`}}

            ></div>
            <article>
              <h1>
              {props.news[6].gameSteamNewsData.title}
                            </h1>
              <span>{props.news[6].gameSteamNewsData.author ? props.news[6].gameSteamNewsData.author : "~unonymus" }</span>
            </article>
          </a>
        </div>
      </div>
    </Fragment>
  );
}

export default NewsHero;
