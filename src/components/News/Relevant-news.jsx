import React from 'react'
import Relnewscomp from '../News/Relevant-news-com'
import { useTranslation, Trans } from 'react-i18next';

import { Fragment } from 'react'
function Relevantnews(props) {
  const { t, i18n } = useTranslation();

  console.log(props);
  return (
    <Fragment>
      
      <div className="news-hero-band">
      <h2 >{t('news.relevantnews')}</h2>
      <div style={{ clear: 'both',backgroundColor:'grey',height:'2px',marginTop:'22px',marginLeft:'-80px'}}></div>
      </div>
      <Relnewscomp news={props.news[7]}/>
      <Relnewscomp news={props.news[8]}/>
      <Relnewscomp news={props.news[9]}/>
    </Fragment>
  )
}

export default Relevantnews