import React from 'react'
import { useTranslation, Trans } from 'react-i18next';

function NewsBanner() {
  const { t, i18n } = useTranslation();

  return (
    <section className="showcase">
    <div className="overlay"></div>
    <div className="text">
      <h2>{t('news.title')}</h2> 
      <h3>{t('news.subtitle')}</h3>
      <p>{t('news.newspar')}</p>
      <a href="#proximamente">{t('news.explore')}</a>
    </div>
    <ul className="social">
      <li><a href="#"><img src="https://i.ibb.co/x7P24fL/facebook.png" alt='facebook'/></a></li>
      <li><a href="#"><img src="https://i.ibb.co/Wnxq2Nq/twitter.png" alt='twitter'/></a></li>
      <li><a href="#"><img src="https://i.ibb.co/ySwtH4B/instagram.png" alt='instagram'/></a></li>
    </ul>
  </section>
  )
}

export default NewsBanner