import React from 'react'

function Relevantnewscom(props) {
  console.log(props);
  return (
    <div className="relevant-news" id='proximamente'>
        <div className="meta">
        <div className="photo" style={{ backgroundImage:`url(${props.news.screenshotUrl})`}}></div>
        </div>
        <div className="description">
        <a href={props.news.gameSteamNewsData.url} target="_blank"><h1>{props.news.gameSteamNewsData.title}</h1></a>
        <h2>{props.news.gameSteamNewsData.author}</h2>
        <p dangerouslySetInnerHTML={{ __html: props.news.gameSteamNewsData.contents}}></p>
        </div>
  </div>
  )
}

export default Relevantnewscom