import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import '../../styles/chat.css';
import Chat from '../User/Chat';

function ChatMessage() {
  const [abrirChat, setAbrirChat] = useState(false);

  const toggleChat = () => {
    setAbrirChat(!abrirChat);
  };

  return (
    <div className='chat-pop'>
      <FontAwesomeIcon icon={faMessage} className='chatty' onClick={toggleChat} />
      {abrirChat && <Chat />}
    </div>
  );
}

export default ChatMessage;
