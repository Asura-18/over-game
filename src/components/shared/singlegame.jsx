import React, { Fragment, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import PuffLoader from "react-spinners/PuffLoader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { faTwitter } from "@fortawesome/free-brands-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import {
  getGameDetailsById,
  gameDetailsSteamWebApi,
  insertUserFavorites,
  gameNewsSteamWebApi,
  getGameStores,
} from "../../services/gameService";
import { useTranslation, Trans } from 'react-i18next';

function Singlegame() {
  const token = localStorage.getItem("authToken");

  const insertFav = async (id) => {
    try {
      await insertUserFavorites({ id }, { token });
    } catch (error) {
      console.error(error);
    }
  };
  const { t, i18n } = useTranslation();

  const [selectedImage, setSelectedImage] = useState(null);
  const [game, setGame] = useState(null);
  const [store, setStores] = useState([]);
  const { id, steamId } = useParams();
  const [showPopup, setShowPopup] = useState(false);
  const [gameNews, setGameNews] = useState([]);

  const handleImageClick = () => {
    setShowPopup(true);
  };

  const imagenelegida = (image) => {
    setSelectedImage(image);
    setShowPopup(true);
  };

  const handleClosePopup = () => {
    setShowPopup(false);
  };

  const findGameNews = async () => {
    const news = await gameNewsSteamWebApi({ steamId });
    setGameNews(news.data);
  };

  const findStores = async () => {
    const strs = await getGameStores({ steamId });
    setStores(strs.data.deals);
    console.log(strs.data);
  };

  const findGame = async () => {
    const gms = await getGameDetailsById({ id });
    setGame(gms.data);
  };

  const [screenshot, setScreenshot] = useState([]);

  const findScreenshots = async () => {
    const scr = await gameDetailsSteamWebApi({ steamId });
    setScreenshot(scr.data.screenshotObjList);
  };

  useEffect(() => {
    findScreenshots();
    findGameNews();
    findGame();
    findStores();
  }, []);

  if (!game) {
    return (
      <div
        style={{
          position: "relative",
          marginTop: "300px",
          left: "40vw",
          marginBottom: "200px",
        }}
      >
        <PuffLoader color="aqua" aria-label="Loading Spinner" size={200} />
      </div>
    );
  }

  return (
    <Fragment>
      <img
        src={
          screenshot.length > 0 && screenshot[0].fullPath
            ? screenshot[0].fullPath
            : "https://rare-gallery.com/uploads/posts/4595853-404-not-found.jpg"
        }
        alt={game.title}
        className="sg-img"
      />
      <div
        className="sgcard-wrapper"
        style={{ position: "relative"}}
      >
        <div className="sgcard">
          {/* div-card-left */}
          <div className="product-imgs">
            <div className="img-display">
              <div className="img-showcase">
                <img
                  src={
                    screenshot.length > 0 && screenshot[1].fullPath
                      ? screenshot[1].fullPath
                      : "https://rare-gallery.com/thumbs/4502659-404-not-found.jpg"
                  }
                  alt={game.title}
                  onClick={() => {
                    imagenelegida(
                      screenshot.length > 0
                        ? screenshot[1].thumbnailPath
                        : "https://wallpaper.dog/large/20565230.jpg"
                    );
                    handleImageClick();
                  }}
                  className="main-img"
                />
              </div>
            </div>
            <div className="img-select">
              <div className="img-item">
                <a href="#" data-id="1">
                  <img
                    src={
                      screenshot.length > 0 && screenshot[0].thumbnailPath
                        ? screenshot[0].thumbnailPath
                        : "https://wallpaper.dog/large/20565230.jpg"
                    }
                    alt={game.title}
                    onClick={() => {
                      imagenelegida(
                        screenshot.length > 0
                          ? screenshot[0].thumbnailPath
                          : "https://wallpaper.dog/large/20565230.jpg"
                      );
                      handleImageClick();
                    }}
                  />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="2">
                  <img
                    src={
                      screenshot.length > 0 && screenshot[2].thumbnailPath
                        ? screenshot[2].thumbnailPath
                        : "https://wallpaper.dog/large/20565230.jpg"
                    }
                    alt={game.title}
                    onClick={() => {
                      imagenelegida(
                        screenshot.length > 0
                          ? screenshot[2].thumbnailPath
                          : "https://wallpaper.dog/large/20565230.jpg"
                      );
                      handleImageClick();
                    }}
                  />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="3">
                  <img
                    src={
                      screenshot.length > 0 && screenshot[3].thumbnailPath
                        ? screenshot[3].thumbnailPath
                        : "https://wallpaper.dog/large/20565230.jpg"
                    }
                    alt={game.title}
                    onClick={() => {
                      imagenelegida(
                        screenshot.length > 0
                          ? screenshot[3].thumbnailPath
                          : "https://wallpaper.dog/large/20565230.jpg"
                      );
                      handleImageClick();
                    }}
                  />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="4">
                  <img
                    src={
                      screenshot.length > 0 && screenshot[4].thumbnailPath
                        ? screenshot[4].thumbnailPath
                        : "https://wallpaper.dog/large/20565230.jpg"
                    }
                    alt={game.title}
                    onClick={() => {
                      imagenelegida(
                        screenshot.length > 0
                          ? screenshot[4].thumbnailPath
                          : "https://wallpaper.dog/large/20565230.jpg"
                      );
                      handleImageClick();
                    }}
                  />
                </a>
              </div>
            </div>
          </div>
          {/* div-card-right */}
          <div className="product-content">
            <h2 className="product-title">{game.gameDto.title}</h2>
            <div className="product-news">
              <p>{game.gameDto.description}</p>
            </div>
            <div className="product-compra">
              <a
                onClick={() => insertFav(game.gameDto.id)}
                className="product-link"
              >
                Añadir al GameDeck
              </a>
            </div>
            <div className="product-social">
              <h2>Comparte este juego en:</h2>
              <a href="#">
                <FontAwesomeIcon icon={faFacebook} />{" "}
              </a>
              <a href="#">
                <FontAwesomeIcon icon={faInstagram} />{" "}
              </a>
              <a href="#">
                <FontAwesomeIcon icon={faTwitter} />{" "}
              </a>
            </div>
          </div>
        </div>
    <div className="other-product-info">
    <div className="stores">
  <h2>{t('games.stores')}</h2>
  <div className="stores-container">
    {store.length > 0 ? (
      store.map((store, index) => (
        <div className="card" key={index}>
          {index === 0 ? (
            <span className="best-offer">Best Offer</span>
          ) : null}
          <div className="card2">
            <h3>{store.storeName}</h3>
            <p>
              RetailPrice: <strong>{store.retailPrice}</strong>
            </p>
            <p>
              Price: <strong>{store.price}</strong>
            </p>
            <p>
              Savings: <strong>{store.savings}</strong>
            </p>
            <span>
              <a
                href={`https://www.cheapshark.com/redirect?dealID=${store.dealID}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                Check out
              </a>
            </span>
          </div>
        </div>
      ))
    ) : (
      <div className="bad-news">{t('games.nostores')}</div>
    )}
  </div>
</div>

<div className="product-details">
  <h2>{t('games.news')}</h2>
  {gameNews.length > 0 ? (
    <div className="table">
      {gameNews.map((news) => (
        <div className="tr" key={news.id}>
          <td>
            <h2>{news.title}</h2>
            <h3>
              Author: <span>{news.author}</span>
            </h3>
            <a href={news.url} target="_blank" rel="noopener noreferrer">
              {news.url}
            </a>
            <p>{news.content}</p>
          </td>
        </div>
      ))}
    </div>
  ) : (
    <div className="bad-news">{t('games.nonews')}</div>
  )}
</div>
    </div>
      </div>
      {showPopup && selectedImage && (
        <div className="popup-overlay">
          <div className="popup-content">
            <button onClick={handleClosePopup}><FontAwesomeIcon icon={faXmark} /></button>
            <img src={selectedImage} alt={game.gameDto.title} />
          </div>
        </div>
      )}
    </Fragment>
  );
}

export default Singlegame;
