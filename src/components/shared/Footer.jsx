import React, { Fragment } from "react";
import '../../styles/Footer.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram} from '@fortawesome/free-brands-svg-icons'
import { faFacebook} from '@fortawesome/free-brands-svg-icons'
import { faTwitter} from '@fortawesome/free-brands-svg-icons'
import { faPlaystation } from "@fortawesome/free-brands-svg-icons";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons";
import { faXbox } from "@fortawesome/free-brands-svg-icons";
import { faLocationDot } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import logo from '../../assets/img/favico/logo_fian5.png'
import { useTranslation, Trans } from 'react-i18next';

function Footer() {
  const { t, i18n } = useTranslation();

  return (
    <Fragment>
      <footer className="footer-distributed">
        <div className="footer-left">
          <h3>
          <img src={logo} style={{width:'200px',marginTop:'10px'}}  alt="Overgame-Logo" />
          </h3>

          <p className="footer-company-name">Overgame</p>
        </div>

        <div className="footer-center">
          <div>
          <FontAwesomeIcon icon={faLocationDot}  className="foot-icon"/>     
            <p>
              <span>IES El Lago</span> Lago, Madrid
            </p>
          </div>

          <div>
          <FontAwesomeIcon icon={faPhone}  className="foot-icon"/>
            <p>
            +34 658 987 654
            <br></br>
            +91 720 368 934
            </p>
          </div>

          <div>
          <FontAwesomeIcon icon={faEnvelope} className="foot-icon"/>          
            <p>
              <a href="mailto:overgame@hotspot.com">Overgame@hotspot.com</a>
            </p>
          </div>
        </div>

        <div className="footer-right">
          <p className="footer-company-about">
            <span>      {t('aboutus.textsubheading')}
</span>
{t('footer.aboutcomppar')}

          </p>

          <div className="footer-icons">
            <a href="/">
            <FontAwesomeIcon icon={faInstagram} style={{backgroundColor:'transparent'}}/>            
            </a>
            <a href="/">
            <FontAwesomeIcon icon={faFacebook} />            
            </a>
            <a href="/">
            <FontAwesomeIcon icon={faTwitter} />            
            </a>
            <a href="/">
            <FontAwesomeIcon icon={faLinkedin} />            
            </a>
            <a href="/">
            <FontAwesomeIcon icon={faPlaystation} />            
            </a>
            <a href="/">
            <FontAwesomeIcon icon={faXbox} />            
            </a>
          </div>
        </div>
        
      </footer>
      <div className="copyright">
      {t('footer.copyright')}
      </div>
    </Fragment>
  );
}

export default Footer;
