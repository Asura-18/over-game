import React, { Fragment } from "react";
import { useState, useEffect } from "react";
import "../../styles/Navbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { faGamepad } from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import logo from "../../assets/img/favico/logo_fian5.png";
import { useTranslation, Trans } from 'react-i18next';

// import logo2 from '../../assets/img/main/logo-2.png'
import UserDropdown from "./UserDropdown";
function Navbar() {
  const { t, i18n } = useTranslation();

  const [mobile, setMobile] = useState(false);
  const [abrirPerfil, setAbrirPerfil] = useState(false);
  const BASE_URL = "http://localhost:3000";
  const authToken = localStorage.getItem("authToken");
  let redirect = "";
  let redirectsettings = "";
  let redirectDeck = "";
  const [searchTerm, setSearchTerm] = useState("");
  let exit = "";
  if (authToken != null) {
    redirect = "/profile";
    redirectDeck = "/profile/game-deck";
    redirectsettings = "/settings";
    exit = "";
  } else {
    redirect = "/Login";
    redirectDeck = "/Login";
    redirectsettings = "/Login";
    exit = "/";
  }

  const [scroll, setScroll] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const handleScroll = () => {
    if (window.pageYOffset > 100) {
      setScroll(true);
    } else {
      setScroll(false);
    }
  };
  const handleSearchInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    window.location.href = `http://localhost:3000/search?title=${searchTerm}`;
  };
  const handleLogout = () => {
    const authToken = localStorage.getItem('authToken');

    if (authToken != null) {
      // Borrar el authToken del localStorage
      localStorage.removeItem('authToken');
      // Redirigir al usuario a '/'
      exit="/";
    } else {
      // Redirigir al usuario a '/profile'
      exit="/Login";
    }
  };
  return (
    <Fragment>
      <header className={scroll ? "header" : "header"}>
        <nav className={scroll ? "scrolled" : "navbar"}>
          <div className="logo">
            <a href="/">
              <p
                style={{ fontFamily: "Oasis" }}
                className={scroll ? "logo-d" : ""}
              >
                <span style={{ color: "white", fontFamily: "Oasis" }}>
                  Over
                </span>
                Game
              </p>
              <img
                src={logo}
                style={{ width: "100px", marginTop: "10px" }}
                className={scroll ? "" : "logo-d"}
                alt="logo"
              />
            </a>
          </div>
          <ul className="links">
            <li>
              <NavLink to="/">{t('navbar.home')}</NavLink>
            </li>
            <li>
              <NavLink to="/games">{t('navbar.games')}</NavLink>
            </li>
            <li>
              <NavLink to="/News">{t('navbar.news')}</NavLink>
            </li>
            <li>
              <NavLink to="/about-us">{t('navbar.aboutus')}</NavLink>
            </li>
          </ul>
          <div className="user-zone">
            <NavLink to={`${BASE_URL}${redirectDeck}`}>
              <FontAwesomeIcon icon={faGamepad} className="cube" />
            </NavLink>
            {authToken ? (
        <NavLink onClick={() => setAbrirPerfil((abierto) => !abierto)}>
          <FontAwesomeIcon icon={faUser} className="user" title="sign in" />
        </NavLink>
      ) : (
        <NavLink to="/Login">
          <FontAwesomeIcon icon={faUser} className="user" title="sign in" />
        </NavLink>
      )}
            <form onSubmit={handleSearchSubmit} className="search-bar-class">
              <input
                type="text"
                placeholder={t('navbar.placeholder')}
                name="title"
                onChange={handleSearchInputChange}
              />
              <button type="submit">
                <FontAwesomeIcon icon={faMagnifyingGlass} />
              </button>
            </form>
            {abrirPerfil && authToken && (
              <UserDropdown
                to={`${BASE_URL}${redirect}`}
                tosettings={`${BASE_URL}${redirectsettings}`}
              />
            )}
          </div>
          <div className="toogle_btn">
            <FontAwesomeIcon
              icon={mobile ? faXmark : faBars}
              onClick={() => setMobile((mobile) => !mobile)}
            />
          </div>
          {mobile && (
            <div className="dropdown_menu">
              <li>
                <NavLink to="/">{t('navbar.home')}</NavLink>
              </li>
              <li>
                <NavLink to="/games">{t('navbar.games')}</NavLink>
              </li>
              <li>
                <NavLink to="/News">{t('navbar.news')}</NavLink>
              </li>
              <li>
                <NavLink to="/about-us">{t('navbar.aboutus')}</NavLink>
              </li>
              <li>
              <NavLink to={`${BASE_URL}${redirectDeck}`}>
              <FontAwesomeIcon icon={faGamepad} className="cube" />
            </NavLink>
              </li>
              <li>
              <NavLink onClick={() => setAbrirPerfil((abierto) => !abierto)}>
              <FontAwesomeIcon icon={faUser} className="user" title="sign in" />
            </NavLink>
              </li>
              {abrirPerfil && (
              <div className="user-propss">
                <li>
                  <NavLink to={`${BASE_URL}${redirect}`}>{t('navbar.profile')}</NavLink>
                </li>
                <li>
                  <NavLink to={`${BASE_URL}${redirectsettings}`}>{t('navbar.settings')}</NavLink>
                </li>
                <li>
                <NavLink onClick={handleLogout} to={`${BASE_URL}/login`}>{t('navbar.logout')}</NavLink>
                </li>
              </div>
            )}
              <li>
                <form onSubmit={handleSearchSubmit} className="search-bar-bar">
                  <input
                    type="text"
                    placeholder={t('navbar.placeholder')}
                    name="title"
                    onChange={handleSearchInputChange}
                  />
                  <button type="submit">
                    <FontAwesomeIcon icon={faMagnifyingGlass} />
                  </button>
                </form>
              </li>
            </div>
          )}
        </nav>
      </header>
    </Fragment>
  );
}

export default Navbar;
