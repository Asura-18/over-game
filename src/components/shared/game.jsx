import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import { faHeartCircleBolt } from "@fortawesome/free-solid-svg-icons";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";
import {
  insertUserFavorites,
  deleteGameById,
} from "../../services/gameService";
function Game(props) {
  const [favredirect, setFavredirect] = useState(false);

  const [role, setRole] = useState(false);
  const token = localStorage.getItem("authToken");

  const insertFav = async (id) => {
    try {
      await insertUserFavorites({ id }, { token });
    } catch (error) {
      console.error(error);
    }
  };
  const deleteGame = async (id) => {
    try {
      await deleteGameById({ id });
      window.location.reload();
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    if (token) {
      const dotIndex = token.indexOf(".");
      const dotIndex2 = token.indexOf(".", dotIndex + 1);
      const encodedSubstring = token.substring(dotIndex + 1, dotIndex2);
      const decodedToken = atob(encodedSubstring);
      const decodedObject = JSON.parse(decodedToken);
      const roles = decodedObject.role[0];
      setFavredirect(true);

      if (roles == "ADMIN") {
        setRole(true);
      }
    }
  }, []);

  return (
    <div className="single-game-container">
      <div className="nft">
        <div className="main-games">
          <img
            className="tokenImage"
            src={
              props.image === "NOT FOUND"
                ? "https://cdn.wallpapersafari.com/89/80/YAtURF.jpg"
                : props.image
            }
            alt={props.title}
          />

          <h3>{props.title}</h3>
          <p className="description">{props.description}</p>
          <div className="tokenInfo">
            <div className="price"></div>
            <div className="duration">
              {favredirect ? (
                <Link onClick={() => insertFav(props.id)}>
                  <FontAwesomeIcon
                    icon={faHeartCircleBolt}
                    fontSize={30}
                    style={{ margin: "20px" }}
                    className="favs"
                  />
                </Link>
              ) : (
                <Link to={'/Login'}>
                  <FontAwesomeIcon
                    icon={faHeartCircleBolt}
                    fontSize={30}
                    style={{ margin: "20px" }}
                    className="favs"
                  />
                </Link>
              )}
              {role && (
                <Link onClick={() => deleteGame(props.id)}>
                  <FontAwesomeIcon
                    icon={faTrashCan}
                    fontSize={30}
                    style={{ margin: "20px" }}
                    className="favs"
                  />
                </Link>
              )}
            </div>
          </div>
          <hr />
          <div className="creator">
            <p>
              <ins>{props.creator}</ins>
            </p>
          </div>
          <button className="boton-juegos">
            <Link to={`/games/${props.id}/${props.steamId}`}>Ver detalles</Link>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Game;
