import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';
import { insertUserFavorites } from "../../services/gameService";

function Game_card_home(props) {
  const token = localStorage.getItem('authToken');
  const insertFav = async (id) => {
  try {
    await insertUserFavorites({id},{token});
  } catch (error) {
    console.error(error);
  }
};
  return (
    <Fragment >
        <div className="card_reservas">
          <img className="card__image" src={props.image} alt={props.title}/>
          <div className="card__content">
            <h3>{props.title}</h3>
            <button>
              <Link to={`/games/${props.id}/${props.steamId}`}>Ver detalles</Link>
              </button>
          </div>
          <div className="card__info">
            <div>
              <i className="material-icons">Price:</i>100€
            </div>
            <div>
              <a onClick={() => insertFav(props.user_id)}  className="card__link">
                <i className="material-icons">Add to bag</i>
              </a>
            </div>
          </div>
        </div>
    </Fragment>
  )
}

export default Game_card_home