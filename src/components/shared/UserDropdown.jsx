import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDoorOpen } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import { useTranslation, Trans } from 'react-i18next';

function UserDropdown(props) {
  const { t, i18n } = useTranslation();

    const navigate = useNavigate();
    let exit="/Login";
    const handleLogout = () => {
      const authToken = localStorage.getItem('authToken');
  
      if (authToken != null) {
        // Borrar el authToken del localStorage
        localStorage.removeItem('authToken');
        // Redirigir al usuario a '/'
        exit="/";
      } else {
        // Redirigir al usuario a '/profile'
        exit="/Login";
      }
    };
  return (
    <div className="dropDownProfile">
        <ul>
        <li><NavLink to={props.to}>{t('navbar.profile')}</NavLink></li>
        <li><NavLink to={props.tosettings}>{t('navbar.settings')}</NavLink></li>
         <li><NavLink onClick={handleLogout} to={exit}> {t('navbar.logout')} <span><FontAwesomeIcon icon={faDoorOpen}/></span></NavLink></li>
        </ul>
    </div>
  )
}

export default UserDropdown