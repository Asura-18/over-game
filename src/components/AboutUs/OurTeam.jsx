import React from 'react'
import carlos from '../../assets/img/other/carlos.png'
import florentin from '../../assets/img/other/florentin.png'
import aron from '../../assets/img/other/aron.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram} from '@fortawesome/free-brands-svg-icons'
import { faFacebook} from '@fortawesome/free-brands-svg-icons'
import { faTwitter} from '@fortawesome/free-brands-svg-icons'
import { faPlaystation } from '@fortawesome/free-brands-svg-icons'
import { useTranslation, Trans } from 'react-i18next';

function OurTeam() {
  const { t, i18n } = useTranslation();

  return (
   <div id='ourteam'>
     <div className="team-wrapper">
      <h1>
      {t('aboutus.teamtitle')}
      </h1>
      <div className="team">
        <div className="team_member">
        <div className="team_img">
          <img src={carlos} alt="CarlosOcampo" />
        </div>
          <h3>Carlos Ocampo</h3>
          <p className="role">Backend API Developer</p>
          <p className='role-desc'>      {t('aboutus.teamcarlos')}
</p>
        </div>
        <div className="team_member">
        <div className="team_img">
          <img src={aron} alt="CarlosOcampo" />
        </div>
        <h3>Aron TJ</h3>
          <p className="role">UI/UX & Front Developer</p>
          <p className='role-desc'>      {t('aboutus.teamaron')}
 </p>
        </div>
        <div className="team_member">
        <div className="team_img">
          <img src={florentin} alt="CarlosOcampo" />
        </div>
        <h3>Florentin Viorel</h3>
          <p className="role">FullStack Developer</p>
          <p className='role-desc'>      {t('aboutus.teamfloren')}
</p>
        </div>
      </div>
     </div>
     <div className="">
  <div className="responsive-container-block Container about-final">
    <p className="text-blk heading ">
    {t('aboutus.thanktitle')}
    </p>
    <p className="text-blk subHeading">
    {t('aboutus.thanksubheading')}
  </p>
    <div className="social-icons-container">
      <a className="social-icon">
      <FontAwesomeIcon className="socialIcon image-block" icon={faInstagram} style={{backgroundColor:'transparent'}}/>            
      </a>
      <a className="social-icon">
      <FontAwesomeIcon className="socialIcon image-block"  icon={faFacebook} style={{backgroundColor:'transparent'}}/>            
      </a>
      <a className="social-icon">
      <FontAwesomeIcon className="socialIcon image-block" icon={faTwitter} style={{backgroundColor:'transparent'}}/>            
      </a>
      <a className="social-icon">
      <FontAwesomeIcon className="socialIcon image-block" icon={faPlaystation} style={{backgroundColor:'transparent'}}/>            
      </a>
    </div>
  </div>
</div>
   </div>
  )
}

export default OurTeam