import React from 'react'

import aboutimg from '../../assets/img/main/zvbj1ryoiptz09af.jpg'
import { useTranslation, Trans } from 'react-i18next';

export default function AboutUsMain() {
  const { t, i18n } = useTranslation();

  return (
    <>
        <div className="responsive-container-block bigContainer">
  <div className="responsive-container-block Container bottomContainer">
    <div className="ultimateImg">
      <img className="mainImg" src={aboutimg} alt='about-image'/>
      <div className="purpleBox">
        <p className="purpleText">
      OVERGAME       
        </p>
      </div>
    </div>
    <div className="allText bottomText">
      <p className="text-blk headingText">
      {t('aboutus.textblk')}
      </p>
      <p className="text-blk subHeadingText">
      {t('aboutus.textsubheading')}
      </p>
      <p className="text-blk description">
      {t('aboutus.textsubpar')}

</p>
      <a className="explore" href='#ourteam'>
      {t('aboutus.textsubteam')}
      </a>
    </div>
  </div>
</div>
    </>
  )
}
