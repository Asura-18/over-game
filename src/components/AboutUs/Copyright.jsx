import React from "react";
import { useTranslation, Trans } from 'react-i18next';

function Copyright() {
  const { t, i18n } = useTranslation();

  return (
    <div className="about-copyright">
      <div className="cp-content">
        <div className="cp-title">{t('aboutus.cophead')}
</div>
        <div className="cp-contents">
        {t('aboutus.coppar')}
          <div className="copy">{t('aboutus.copvalve')}</div>
        </div>
      </div>
    </div>
  );
}

export default Copyright;
