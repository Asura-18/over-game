import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram} from '@fortawesome/free-brands-svg-icons'
import { faFacebook} from '@fortawesome/free-brands-svg-icons'
import { faTwitter} from '@fortawesome/free-brands-svg-icons'
import { useTranslation, Trans } from 'react-i18next';

function AboutUsHero() {
  const { t, i18n } = useTranslation();

  return (
    <div className='about-header-container'>
        <div className="about-header">
            <h1>
            <span className="site-title">{t('aboutus.hero1')}</span>
            <span className="site-desc">{t('aboutus.hero2')}</span>            
            </h1>
            <div className="about-icons">
            <a href="https://www.instagram.com/" className="about-icon">
                <FontAwesomeIcon icon={faInstagram} style={{backgroundColor:'transparent'}}/>            
            </a>
            <a href="https://www.facebook.com/" className="about-icon">
            <FontAwesomeIcon icon={faFacebook} />            
            </a>
            <a href="https://www.twitter.com/" className="about-icon">
            <FontAwesomeIcon icon={faTwitter} />            
            </a>
            </div>
        </div>
    </div>
  )
}

export default AboutUsHero