import React,{useState} from 'react'
import { useTranslation, Trans } from 'react-i18next';

const faqs = [
    {
      question: '¿Cómo puedo descargar la aplicación Overgame?',
      answer: 'Puedes descargar la aplicación Overgame desde el sitio web oficial de Overgame o desde las tiendas de aplicaciones de tu dispositivo móvil. Simplemente busca "Overgame" en la tienda correspondiente y sigue los pasos para descargar e instalar la aplicación en tu dispositivo.'
    },
    {
      question: '¿Necesito crear una cuenta para usar Overgame?',
      answer: 'No es necesario, pero , es necesario crear una cuenta en Overgame para acceder a todas las funciones de la aplicación. Puedes crear una cuenta de forma gratuita proporcionando algunos detalles básicos y eligiendo un nombre de usuario y una contraseña.'
    },
    {
      question: '¿Puedo comprar juegos a través de Overgame?',
      answer: 'No, Overgame es una aplicación que te permite acceder y gestionar tu biblioteca de juegos de Overgame. Para comprar juegos, necesitarás tener una cuenta en Overgame y realizar las compras a través de la plataforma oficial de Overgame.'
    },
    {
      question: '¿Cómo puedo agregar juegos a mi biblioteca en Overgame?',
      answer: 'Para agregar juegos a tu biblioteca en Overgame, debes iniciar sesión con tu cuenta de Overgame en la aplicación. Una vez que hayas iniciado sesión, Overgame sincronizará automáticamente tu biblioteca de juegos de Overgame y mostrará todos tus juegos disponibles.'
    },
    {
      question: '¿Puedo jugar juegos a través de Overgame?',
      answer: 'No, Overgame no es una plataforma de juego. Solo te permite acceder y administrar tu biblioteca de juegos de Overgame. Para jugar los juegos, necesitarás tener instalado el cliente de Overgame en tu dispositivo y ejecutar los juegos a través de la plataforma de Overgame.'
    }
  ];
 function Faq() {
  const { t, i18n } = useTranslation();

   const[accordion,setAccordion]=useState(-1);
   function toogleAccordion(index){
    setAccordion(index);
   }
  return (
    <div className='about-team-container' id='about-us'>
    <div>
        <span className="accordion_title">
        {t('aboutus.faqtitle')}
            <h1> {t('aboutus.faqsubtitle')}</h1>
        </span>
    </div>
    <div className="accordion_faq">
        {faqs.map((item,index)=>
        <div className="accordion_item" key={index} onClick={()=>toogleAccordion(index)}>
            <div className="accoridion_faq_heading">
                <h3 className={accordion===index?"active":""}>{item.question}</h3>
            </div>
            <div>
            {accordion===index?(
                <>
                <span className="verticle">-</span>
                </>
            ):(
                <>
                  <span className="verticle">+</span>
                </>
            )}
            </div>
            <div className="accoridion_faq_content">
                <p className={accordion===index?"active":"inactive"}>{item.answer}</p>
            </div>
        </div>
        )}
    </div>
</div>
  )
}
export default Faq