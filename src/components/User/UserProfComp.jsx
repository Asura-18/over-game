import React, {useState, useEffect, Fragment} from "react";
import prof from "../../assets/img/favico/ZTXNs73bym-1.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {getGameDetailsById, getPfp} from "../../services/gameService";
import {faPencil} from "@fortawesome/free-solid-svg-icons";
import {faHeartBroken} from "@fortawesome/free-solid-svg-icons";
import {NavLink} from "react-router-dom";
import Mideck from "./Mi-deck";
import {getFriends} from '../../services/gameService'
import {friendRemove} from "../../services/gameService";
import Friends from './Friends';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { useTranslation, Trans } from 'react-i18next';

function UserProfComp(props) {
    const { t, i18n } = useTranslation();

    const [role,setRole]=useState(false);
    const token = localStorage.getItem("authToken");
    const [activeTab, setActiveTab] = useState("mi-deck");
    const [userFriends, setUserFriends] = useState([]);
    const findUserFriends = async () => {
        const us_friends = await getFriends({token});
        setUserFriends(us_friends.data);
    };
   
    const handleTabClick = (tab) => {
        setActiveTab(tab);
    };
    const [frRequests, setFrRequests] = useState([]);

    const removeUser = async (id) => {
        try {
            await friendRemove({token}, {id: id});
            const updatedFrRequests = frRequests.filter(
                (request) => request.id !== id
            );
            setFrRequests(updatedFrRequests);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        findUserFriends();
        if(props.role=="ADMIN"){
            setRole(true);
        }
    }, []);



    // Works to get the pfp
    const [pfp, setPfp] = useState([]);
    const findProfile = async () => {
        try {
            const image = await getPfp({token});
            setPfp(image);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        findProfile();
    }, []);

    return (
        <div className="user-profiler">
            <div className="header_wrapper">
                <header></header>
                <div className="cols_container">
                    <div className="left_col">
                        <div className="img_container">
                            <img
                                src={pfp ? pfp : prof}
                                alt="user-profile"
                            />
                        </div>
                        <h2>{props.name}</h2>
                        <p>{props.email ? props.email : "Inserta tu email"}</p>
                        <ul className="about">
                            <li className="about-me"> 
                                <span>{props.friends} </span>{t('user.amigos')}
                            </li>
                            <li className="about-me">
                                <span>{props.friendsRequest} </span>{t('user.requests')}
                            </li>
                            <li className="about-me">
                                <span>{props.favorites}</span>{t('user.favoritos')}
                            </li>
                        </ul>
                        <p>
                            {props.description
                                ? props.description
                                : "Hazte conocer a los usuarios, escribe ya tu desrcipción."}
                        </p>
                    </div>
                    <div className="right_col">
                        <section className="links">
                            <ul>
                                <li>
                                    <a onClick={() => handleTabClick('mi-deck')}>{t('user.mideck')}</a>
                                </li>
                                <li>
                                    <a onClick={() => handleTabClick('friends')}>{t('user.friends')}</a>
                                </li>
                            </ul>
                        </section>
                        <section className="prog-games">
                            {activeTab === "mi-deck" && <Mideck/>}
                            {activeTab === "friends" && <Friends token={token}/>}
                        </section>
                    </div>
                </div>
                <div className="courses-container" id="friend-container">
                    <h2>{t('user.friends')}</h2>
                    {userFriends.length > 0 ? (
                        userFriends.map((friend) => {
                            return (
                                <div className="course" key={friend.id}>
                                    <div className="course-preview">
                                        <h6>{t('user.user')}</h6>
                                        <h3>{friend.username}</h3>
                                    </div>
                                    <div className="course-info">
                                        <button className="btnmas" onClick={() => removeUser(friend.id)}>
                                            <FontAwesomeIcon icon={faHeartBroken}/>
                                        </button>
                                    </div>
                                </div>
                            );
                        })
                    ) : (
                        <div id="sin-amigos">
                            <p>{t('user.nofriends')}</p>
                        </div>
                    )}
                </div>
            </div>
            {role &&  (

                <Fragment>
   <div className="create-game">
                <NavLink to={"/create-game"}>
                    <FontAwesomeIcon icon={faPencil} id="pencil-create"/>
                </NavLink>
            </div>
            <div className="remove-game">
                    <NavLink to={"/search?title="}>
                    <FontAwesomeIcon icon={faTrashAlt} id="remove"/>
                    </NavLink>
            </div>
                </Fragment>

            )
            }
         
        </div>
    );
}

export default UserProfComp;
