import React, {useEffect, useRef, useState} from "react";
import {Client} from "@stomp/stompjs";
import SockJS from "sockjs-client";
import {findCurrentUser} from '../../services/gameService'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { useTranslation, Trans } from 'react-i18next';

const MyChat = (props) => {
    const { t, i18n } = useTranslation();

    const [userData, setUserData] = useState({});
    const [friends, setFriends] = useState([]);
    const [friendsConnected, setFriendsConnected] = useState([]);
    const [messageReceived, setMessageReceived] = useState([]);
    const [currentMessage, setCurrentMessage] = useState("");
    const [selectedFriend, setSelectedFriend] = useState(null);
    const stompClientDestination = useRef(null);
    const isSubscribed = useRef(false);

    const token = localStorage.getItem("authToken");

    useEffect(() => {
        const fetchData = async () => {
            const response = await findCurrentUser({ token });
            setFriends(response.data.friends);
            setUserData({
                senderName: response.data.username,
                receiverName: "nobody yet",
                connected: true,
                message: "Me he unido al chat",
            });
        };
        fetchData();

        const localStorageData = JSON.parse(localStorage.getItem('messageReceived'));
        if(localStorageData) {
            setMessageReceived(localStorageData);
        }

        const connect = () => {
            const socket = new SockJS("http://localhost:8080/websocket-server");
            const stompClient = new Client({
                webSocketFactory: () => socket,
                onConnect: () => {
                    if (!isSubscribed.current) {
                        stompClient.subscribe("/chatroom/public", (payload) =>
                            setFriendsConnected(JSON.parse(payload.body))
                        );
                        stompClientDestination.current.publish({
                            destination: "/app/message",
                            body: JSON.stringify(userData)
                        });

                        isSubscribed.current = true;
                    }
                    stompClient.subscribe(
                        `/user/${userData.senderName}/private`,
                        (payload) => {
                            const newMessage = JSON.parse(payload.body);
                            setMessageReceived((prevMessages) => {
                                const updatedMessages = [...prevMessages, newMessage];
                                localStorage.setItem('messageReceived', JSON.stringify(updatedMessages));
                                return updatedMessages;
                            });
                        }
                    );
                },
            });
            stompClient.activate();
            stompClientDestination.current = stompClient;
        };

        connect();

        return () => {
            isSubscribed.current = false;
        };
    }, [userData.senderName, token]);

    const handleSendMessage = () => {
        stompClientDestination.current.publish({
            destination: "/app/private-message",
            body: JSON.stringify({ ...userData, message: currentMessage }),
        });
        setCurrentMessage("");
    };

    return (
        <div className="chat-container">
            <div className="friends-list">
                <h3>{t('chat.friends')}</h3>
                {friends.map((friend) => (
                    <p
                        key={friend.id}
                        className={`friend ${friend.username === selectedFriend ? "selected" : ""}`}
                        onClick={() => {
                            setSelectedFriend(friend.username);
                            setUserData((prevData) => ({
                                ...prevData,
                                receiverName: friend.username,
                            }));
                        }}
                    >
                        {friend.username} 
                        {/* -
                         {friendsConnected.includes(friend.username) ? "conectado" : "desconectado"} */}
                    </p>
                ))}
            </div>
            <div className="chat-messages">
                <h4>{t('chat.messages')}</h4>
                <div className="mensajes-recibidos">
                     {messageReceived.map((message, index) => (
                    (message.senderName === userData.senderName &&
                        message.receiverName === selectedFriend) ||
                    (message.receiverName === userData.senderName &&
                        message.senderName === selectedFriend) ? (
                        <p key={index}>
                            {message.senderName}: {message.message}
                        </p>
                    ) : null
                ))}
                </div>
               
                <div className="mensajes">
                     <input
                    type="text"
                    className="chat-input"
                    value={currentMessage}
                    onChange={(e) => setCurrentMessage(e.target.value)}
                    placeholder="Enter message"
                />
                <button
                    type="button"
                    className="chat-button"
                    onClick={handleSendMessage}
                >
                <FontAwesomeIcon icon={faPaperPlane} />
                </button>
                </div>
               
            </div>
        </div>
    );
};

export default MyChat;
