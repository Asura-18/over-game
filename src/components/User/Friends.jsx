import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import {
  getFrRequests,
  friendConfirm,
  friendDecline,friendAdd
} from "../../services/gameService";
import axios from "axios";
import { useLocation } from "react-router-dom";
import ScaleLoader from "react-spinners/ScaleLoader";
import { useTranslation, Trans } from 'react-i18next';

function Friends(props) {
  const { t, i18n } = useTranslation();
  const location = useLocation();
  const [searchTerm, setSearchTerm] = useState("");
  const [friends, setFriends] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isSearching, setIsSearching] = useState(false); // Variable de estado para controlar si se está realizando una búsqueda

  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const title = searchParams.get("title");

    setSearchTerm(title || "");

    if (title) {
      searchFriends(title);
    }
  }, [location]);

  const searchFriends = async (query) => {
    try {
      setIsLoading(true);
      setIsSearching(true); // Marcar como búsqueda en progreso
      const response = await axios.get(
        `http://localhost:8080/admin/rest/users?username=${query}`
      );
      const frienddata = response.data;
      setFriends(frienddata);
      setIsLoading(false);
    } catch (error) {
      console.error(error);
    } finally {
      setIsSearching(false); // Marcar como búsqueda completada
    }
  };

  const handleSearchInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    searchFriends(searchTerm);
  };



  const [frRequests, setFrRequests] = useState([]);
  const { token } = props;

  const confirmUser = async (id) => {
    try {
      await friendConfirm({ token }, { id: id });
      const updatedFrRequests = frRequests.filter(
        (request) => request.id !== id
      );
      setFrRequests(updatedFrRequests);
    } catch (error) {
      console.log(error);
    }
  };
  const addUser = async (id) => {
    try {
      await friendAdd({ token }, { id: id });
    } catch (error) {
      console.log(error);
    }
  };

  const declineUser = async (id) => {
    try {
      await friendDecline({ token }, { id: id });
      const updatedFrRequests = frRequests.filter(
        (request) => request.id !== id
      );
      setFrRequests(updatedFrRequests);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const getFrRequestsData = async () => {
      try {
        const frReq = await getFrRequests({ token });
        setFrRequests(frReq.data);
      } catch (error) {
        console.log(error);
      }
    };

    getFrRequestsData();
  }, [token]);


  return (
    <div className="friends">
      <div className="box">
        <form onSubmit={handleSearchSubmit}>
          <input
            type="text"
            className="input"
            name="txt"
            required
            value={searchTerm}
            onChange={handleSearchInputChange}
          />
          <button type="submit">
            <i>
              <FontAwesomeIcon icon={faMagnifyingGlass} />
            </i>
          </button>
        </form>
      </div>
      <div style={{ margin: "20px" }}>
        {isLoading ? (
          <div>
          </div>
        ) : (
          <div className="courses-container">
            {friends && friends.length > 0 ? (
              friends.map((friend) => (
                <div className="course" key={friend.id}>
                  <div className="course-preview">
                    <h6>{t('user.user')}</h6>
                    <h3>{friend.username}</h3>
                  </div>
                  <div className="course-info">
                    <button className="btnmas favs-friend"  onClick={() => addUser(friend.id)}>+</button>
                  </div>
                </div>
              ))
            ) : (
              <div className="no-games-found">
                <p></p>
              </div>
            )}
          </div>
        )}
      </div>
      <div className="courses-container">
        <h2>{t('user.fr')}</h2>
        {frRequests.length > 0 ? (
          frRequests.map((request) => (
            <div className="course" key={request.id}>
              <div className="course-preview">
                <h6>{t('user.user')}</h6>
                <h3>{request.username}</h3>
              </div>
              <div className="course-info">
                <button
                  className="btnmas favs-friend"
                  onClick={() => confirmUser(request.id)}
                >
                  +
                </button>
                <button
                  className="btnmenos favs-friend"
                  onClick={() => declineUser(request.id)}
                >
                  -
                </button>
              </div>
            </div>
          ))
        ) : (
          <div>
            <p>{t('user.nosol')}</p>
          </div>
        )}
      </div>
    </div>
  );
}


export default Friends;
