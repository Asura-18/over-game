import React,{useState,useEffect} from "react";
import form_cr_image from "../../assets/img/other/7671274_1624.jpg";
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from "axios";
import { createGame } from "../../services/gameService";
function CreateGameForm() {

  const navigate = useNavigate();
  const token = localStorage.getItem('authToken');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [steamId, setSteamId] = useState('');
  const [showModal, setShowModal] = useState(false);

  const gamecreation = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:8080/game/new', {
        title: title,
        description: description,
        steamId:steamId
      },
      {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      });
      if (response.status === 200) {
        toast.success('Registrado con éxito', {
          position: "top-right",
          autoClose: 12000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "colored",
        });

      }
      setShowModal(true);
      navigate('/profile');
    } catch (error) {
      console.log(error);
      toast.error('Error al registrar el juego', {
        position: "top-right",
        autoClose: 12000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
      setShowModal(true);
    }
};





  return (
    // <div className="form-cr-game">
    //   <form onSubmit={gamecreation}>
    //     {/* form groups to output are: game name/title , game desc, and steam id. */}
    //     <div className="form-container">
    //       <h1>Create a new game</h1>

    //       <div className="form-group">
    //         <input
    //           type="text"
    //           className="form-control"
    //           id="game-name"
    //           required value={title}
    //           onChange={(e) => setTitle(e.target.value)}/>
    //         <label htmlFor="game-name" >Game Name</label>
    //       </div>

    //       <div className="form-group">
    //         <textarea
    //           type="text"
    //           className="form-control"
    //           id="game-description"
    //           maxlength="150"
    //           required value={description}
    //           onChange={(e) => setDescription(e.target.value)}/>
    //         <label htmlFor="game-description">Game Description (max-150)</label>
    //    </div>
    //       <div className="form-group">
    //         <input
    //           type="number"
    //           className="form-control"
    //           id="game-steam-id"
    //           required value={steamId}
    //           onChange={(e) => setSteamId(e.target.value)} />
    //         <label htmlFor="steam-id">Steam Id</label>
    //       </div>
    //       <div className="form-group btn-glitch-fill">
    //       <input  type="submit"  className="text"  value="Create"/>
    //           <span className="decoration"></span>
    //       </div>
    //     </div>
    //     <img src={form_cr_image} />
    //   </form>
    //   {showModal && (
    //     <ToastContainer
    //     position="top-right"
    //     autoClose={12000}
    //     hideProgressBar={false}
    //     newestOnTop={false}
    //     closeOnClick
    //     rtl={false}
    //     pauseOnFocusLoss
    //     draggable
    //     pauseOnHover
    //     theme="colored"
    //     />
    //   )}
    // </div>
    <div>
    <div className="form-us-settings">
        <form onSubmit={gamecreation}>
            <div className="form-container">
            <h1>Create a new game</h1>

                <div className="form-group">
                    <input
                        type="text"

                        className="form-control"
                        required value={title}
                       onChange={(e) => setTitle(e.target.value)}/>
                    <label htmlFor="game-name">Game Name</label>
                </div>
                <div className="form-group">
                    <input
                        type="number"
                        required value={steamId}
                        onChange={(e) => setSteamId(e.target.value)}
                    />
                    <label htmlFor="game-description">Steam Id</label>
                </div>
                <div className="form-group">
    <textarea
        type="text"
        className="form-control"
        id="game-description"
        maxLength="150"
                 required value={description}
          onChange={(e) => setDescription(e.target.value)}
    />
                    <label htmlFor="game-description">
                        Game Description (max-150)
                    </label>
                </div>
               
                <div className="form-group btn-glitch-fill">
                    <input
                        type="submit"
                        className="text"
                        value="Create"
                    />
                    <span className="decoration"></span>
                </div>
            </div>
            <div id="create-game">
               
            </div>
        </form>
</div>
{showModal && (
    <ToastContainer
        position="top-right"
        autoClose={12000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
    />
)}
</div>
  );
}

export default CreateGameForm;
