import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {getPfp, getUserDetails, setPfp} from '../../services/gameService';
import prof from '../../assets/img/favico/ZTXNs73bym-1.png';
import {useNavigate} from 'react-router-dom';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import ScaleLoader from 'react-spinners/ScaleLoader';
import { useTranslation, Trans } from 'react-i18next';

function Settings() {
    const { t, i18n } = useTranslation();

    const navigate = useNavigate();
    const token = localStorage.getItem('authToken');
    const dotIndex = token.indexOf('.');
    const dotIndex2 = token.indexOf('.', dotIndex + 1);
    const encodedSubstring = token.substring(dotIndex + 1, dotIndex2);
    const decodedToken = atob(encodedSubstring);
    const decodedObject = JSON.parse(decodedToken);
    const id = decodedObject.id;
    const [userdetails, setUserdetails] = useState(null);

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [description, setDescription] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [picture, setPicture] = useState(null);

    const findUserDetails = async () => {
        try {
            const us_dets = await getUserDetails({id}, {token});
            setUserdetails(us_dets.data);
        } catch (error) {
            console.log(error);
        }
    };
    const findProfile = async () => {
        try {
            const image = await getPfp({token});
            setPicture(image);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        findUserDetails();
        findProfile();
    }, []);


    // Lab to set the pfp (el estado por defecto api get pfp)

    const handleSubmit = async (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('profilePic', picture);
        //console.log(formData.get('profilePic'));
        try {
            await setPfp({formData}, {token});
        } catch (error) {
            console.log(error);
        }
    };

    const handlePictureChange = (event) => {
        setPicture(event.target.files[0]);
    };

    const userupdate = async (e) => {
        e.preventDefault();
        try {
            console.log(username, description, email, password);
            const responsedetails = await axios.put(
                'http://localhost:8080/admin/rest/user/' + id,
                {
                    "username": username,
                    "password": password,
                    "email": email,
                    "description": description
                },
                {
                    headers: {
                        'Authorization': 'Bearer ' + token,
                        'Content-Type': 'application/json',
                    }
                }
            );
            await handleSubmit(e);
            if (responsedetails.status === 200) {
                toast.success('Registrado con éxito', {
                    position: 'top-right',
                    autoClose: 12000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: 'colored'
                });
                //navigate('/profile');
            }
            setShowModal(true);
        } catch (error) {
            console.log(error);
            toast.error('Error al registrar el juego', {
                position: 'top-right',
                autoClose: 12000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: 'colored'
            });
            setShowModal(true);
        }
    };




    return (
        <div>
            <div className="form-us-settings">
                {userdetails ? (
                    <form onSubmit={userupdate}>
                        <div className="form-container">
                            <h1>{t('forms.usersettings')}</h1>

                            <div className="form-group">
                                <input
                                    type="text"

                                    className="form-control"
                                    id="game-name"
                                    placeholder={userdetails.username}
                                    value={username}
                                    onChange={(e) => setUsername(e.target.value)}
                                />
                                <label htmlFor="game-name">{t('forms.username')}</label>
                            </div>
                            <div className="form-group">
                                <input
                                    type="password"
                                    id="user-password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                                <label htmlFor="game-description">{t('forms.password')}</label>
                            </div>
                            <div className="form-group">
                <textarea
                    type="text"
                    className="form-control"
                    id="game-description"
                    maxLength="150"
                    placeholder={userdetails.description}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                                <label htmlFor="game-description">
                                {t('forms.description')} (max-150)
                                </label>
                            </div>
                            <div className="form-group">
                                <input
                                    type="email"
                                    id="user-email"
                                    placeholder={userdetails.email}
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    pattern='^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$'
                                />
                                <label htmlFor="game-description">{t('forms.email')}</label>
                            </div>
                            <div className="form-group btn-glitch-fill">
                                <input
                                    type="submit"
                                    className="text"
                                    value="Create"
                                />
                                <span className="decoration"></span>
                            </div>
                        </div>
                        <div>
                            <img
                                src={picture ? picture : prof}
                                alt={userdetails.username}
                            />
                            <div className="user-prof">
                                <input
                                    type="file"
                                    id="fichero"
                                    onChange={handlePictureChange}
                                />
                                <label htmlFor="fichero">{t('forms.subir')}</label>
                            </div>
                        </div>
                    </form>
                ) : (
                    <div className="div">
                         <div className="loading-games">
        <ScaleLoader  color="aqua" aria-label="Loading Spinner" style={{position:'relative'}} height={210} width={48} />
      </div> 
                    </div>
                )}
            </div>
            {showModal && (
                <ToastContainer
                    position="top-right"
                    autoClose={12000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="colored"
                />
            )}
        </div>
    );
}

export default Settings;
