import React,{useState,useEffect} from 'react'
import { getUserFavourites } from "../../services/gameService";
import { getGameDetailsById } from "../../services/gameService";
import { useTranslation, Trans } from 'react-i18next';

function Mideck() {
  const { t, i18n } = useTranslation();

    const [favourites, setFavourites] = useState([]);
    const [gameDetails, setGameDetails] = useState([]);
    const token = localStorage.getItem("authToken");
    const findFavorites = async () => {
      const favs = await getUserFavourites({ token });
      setFavourites(favs.data);
    };
    const findGameDetails = async () => {
      const gameDetailsPromises = favourites.map(async (id) => {
        const response = await getGameDetailsById({ id });
        return response.data;
      });
  
      const gameDetails = await Promise.all(gameDetailsPromises);
      setGameDetails(gameDetails);
    };
  
    useEffect(() => {
      findFavorites();
      console.log(favourites);
    }, []);
    // const imageOptions = [prof, prof,prof3]; 
    // const randomImage = imageOptions[Math.floor(Math.random() * imageOptions.length)];
    useEffect(() => {
      if (favourites.length > -1) {
        findGameDetails();
        console.log(gameDetails);
      }
    }, [favourites]);
  return (
    <div className="games-deck-user">
    {/* <h3>Game Deck</h3> */}
    <ul>
      {gameDetails.length > 0 ? (
        gameDetails.map((game) => <li className="juegos" style={{backgroundImage: `url("${game.screenshot ? game.screenshot : "https://1.bp.blogspot.com/-RTHwUFDJNvg/X20Tzko5WaI/AAAAAAAAFkQ/r_Uke7B0134uHcxMuVXpvKCpmH0iwUrTgCNcBGAsYHQ/s1600/0.jpg"}")`,backgroundSize:'cover'}}> <span>{game.gameDto.title}</span> </li>)
      ) : (
        <li className="sin_juegos">{t('user.sinjuegos')}</li>
      )}
    </ul>
    </div>
  )
}

export default Mideck