import React from 'react'
import {Routes,Route,useLocation } from 'react-router-dom';
import Home from '../../pages/Home';
import Login from '../../pages/Login'
import News from '../../pages/News'
import Err404 from '../../pages/Err-404'
import Register from '../../pages/Register';
import Games from '../../pages/games';
import Game from '../../pages/Game';
import Test from '../../pages/test';
import CreateGame from '../../pages/Create-Game';
import UserProfile from '../../pages/User-Profile'
import UserCart from '../../pages/UserCart';
import AboutUs from '../../pages/AboutUs';
import SearchPage from '../../pages/SearchPage';
import SettingsPage from '../../pages/SettingsPage';
function Sliderroutes() {
    const location=useLocation();
  return (
    <Routes location={location} key={location.pathname}>
          <Route index path='/' element={<Home/>}/>
          <Route path='/News' element={<News/>}/>
          <Route path='/Login' element={<Login/>}/>
          <Route path='/games' element={<Games/>}/>
          <Route path='/*' element={<Err404/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/games/:id/:steamId' element={<Game/>}/>
         {/* <Route path='/test' element={<Test/>}/> */}
         <Route path='/create-game' element={<CreateGame/>}/>
         <Route path='/profile' element={<UserProfile/>}/>
         <Route path='/profile/game-deck' element={<UserCart/>}/>
         <Route path='/about-us' element={<AboutUs/>}/>
         <Route path='/search' element={<SearchPage/>}/>
         <Route path='/settings' element={<SettingsPage/>}/>
    </Routes>

  )
}

export default Sliderroutes