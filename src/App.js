import './App.css';
import {useState,useEffect} from 'react'
import { BrowserRouter} from 'react-router-dom';
import BarLoader    from "react-spinners/BarLoader";
import { useTranslation, Trans } from 'react-i18next';
// import Home from './pages/Home';
// import Browser from './pages/Browser'
// import Login from './pages/Login'
// import News from './pages/News'
// import PC from './pages/PC'

// import Games from './pages/games';
// import Err404 from './pages/Err-404'
import Sliderroutes from './components/SliderRoutes/Sliderroutes';
const lngs = {
  en: { nativeName: 'English' },
  es: { nativeName: 'Spanish' }
};
function App() {
    const { t, i18n } = useTranslation();
  const up = ()=>{
    window.scrollTo(0, 0);
  }
  const[loading,setLoading] = useState(false);
  useEffect(() => {
    const handleBeforeUnload = (event) => {
      if (event.clientY < 0) {
        localStorage.removeItem('authToken');
      }
    };

    window.addEventListener('beforeunload', handleBeforeUnload);

    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);
  useEffect(()=>{
    setLoading(true)
    setTimeout(()=>{
      setLoading(false)
    },2500)
  },[])
  return (
    <div className="App">
      {
        loading ?
        <div className='loader'>
           <p>Over<span>Game</span>
           <BarLoader color="aqua" height={10} width={200} id='loader-icon'/>          
            </p>
        </div>

        :
        
        <BrowserRouter>
        <Sliderroutes/>
        </BrowserRouter>
      }
        
    </div>
  );
}

export default App;
